openssl pkcs12 -nocerts -out APNSKey.pem -in key.p12

openssl x509 -in dev_aps.cer -inform der -out devAPNSCert.pem

cat devAPNSCert.pem APNSKey.pem > devAPNSCertAndKey.pem

rm devAPNSCert.pem

rm APNSKey.pem
