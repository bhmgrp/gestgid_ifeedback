//
//  SidebarTableViewCell.h
//  Pickalbatros
//
//  Created by User on 09.06.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SidebarTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *pointLabel;
@property (weak, nonatomic) IBOutlet UILabel *messagesAmountLabel;
@property (weak, nonatomic) IBOutlet UIView *messagesAmountLabelBackground;

@property (weak, nonatomic) IBOutlet UIImageView *itemImageView;


@end
