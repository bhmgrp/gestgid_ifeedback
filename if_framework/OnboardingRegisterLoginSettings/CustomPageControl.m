//
// Created by Nicolas Tichy on 2/5/15.
// Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "CustomPageControl.h"

@implementation CustomPageControl
{
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
        //self.activeImage = [UIImage imageNamed:@"activedot"];
        //self.inactiveImage = [UIImage imageNamed:@"inactivedot"];
    }
    return self;
}

-(void)updateDots
{
    for (int i = 0; i < [self.subviews count]; i++)
    {
        UIView* dot = self.subviews[i];
        //UIImageView * newDot = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 13, 13)];
        if (i == self.currentPage)
        {
            //newDot.image = self.activeImage;
            dot.backgroundColor = [UIColor colorWithRGBHex:CUSTOMERCOLOR];
        }
        else
        {
            dot.backgroundColor = [UIColor colorWithWhite:1.0f alpha:1.0f];
            //newDot.image = self.inactiveImage;
        }
        //[dot addSubview:newDot];
        //dot.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.0f];
        
    }
}

-(void)setCurrentPage:(NSInteger)page
{
    [super setCurrentPage:page];
    [self updateDots];
}

@end