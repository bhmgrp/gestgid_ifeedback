//
// Created by Nicolas Tichy on 2/5/15.
// Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface OnboardingCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (weak, nonatomic) IBOutlet UIButton *fbButton;

@end