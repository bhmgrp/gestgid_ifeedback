//
//  RegisterTableViewCell.h
//  KAMEHA
//
//  Created by User on 19.02.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RegisterTextField.h"

@protocol registerCellDelegate <NSObject>

-(void)switchSwitched:(BOOL)selected;
-(void)confirmButtonClicked;

@end

@interface RegisterTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelDescription;
@property (weak, nonatomic) IBOutlet RegisterTextField *textInput;
@property (weak, nonatomic) IBOutlet UISwitch *cellSwitch;
@property (weak, nonatomic) id<registerCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIButton *confirmButton;
- (IBAction)confirmButtonClicked:(id)sender;

@property (nonatomic, strong) NSString* textToSaveTo;

@property (weak, nonatomic) IBOutlet UIButton *saveButton;

@property (weak, nonatomic) IBOutlet UIView *uploadImageView;
@property (weak, nonatomic) IBOutlet UIImageView *uploadedImageView;
@property (weak, nonatomic) IBOutlet UIImageView *uploadPlaceHolderImage;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *uploadImageActivityIndicator;
@property (weak, nonatomic) IBOutlet UILabel *welcomeLabel;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *uploadImageLabel;

@property (weak, nonatomic) IBOutlet UIButton *fbButton;
@property (weak, nonatomic) IBOutlet UILabel  *orLabel;

@property (weak, nonatomic) IBOutlet UILabel *privacyPolicyLabel;
@property (weak, nonatomic) IBOutlet UIView *privacyPolicyOverlayView;

@end
