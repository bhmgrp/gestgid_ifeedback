/*
 * Copyright (c) 2011-2012 Matthijs Hollemans
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#import "if_home.h"

#import "gestgidStartViewController.h"
#import "NAvigationViewController.h"
#import "StructureOverViewController.h"

static const NSInteger TagOffset = 1000;


@interface if_home (){
    int numOfTabs;
}

@property (nonatomic) StructureOverViewController *startViewController;
@property (nonatomic) NSMutableArray *customViewControllers;

@end

@implementation if_home
{
    UIView *tabButtonsContainerView;
    UIView *contentContainerView;
    UIImageView *indicatorImageView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initViewControllers];
    
    self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    CGRect rect = CGRectMake(0.0f, 0.0f, self.view.bounds.size.width, self.tabBarHeight);
    tabButtonsContainerView = [[UIView alloc] initWithFrame:rect];
    tabButtonsContainerView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:tabButtonsContainerView];
    
    rect.origin.y = self.tabBarHeight;
    rect.size.height = self.view.bounds.size.height - self.tabBarHeight;
    contentContainerView = [[UIView alloc] initWithFrame:rect];
    contentContainerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:contentContainerView];
    
    indicatorImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"MHTabBarIndicator"]];
    [self.view addSubview:indicatorImageView];
    
    [self reloadTabButtons];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    [self layoutTabButtons];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Only rotate if all child view controllers agree on the new orientation.
    for (UIViewController *viewController in self.viewControllers)
    {
        if (![viewController shouldAutorotateToInterfaceOrientation:interfaceOrientation])
            return NO;
    }
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    if ([self isViewLoaded] && self.view.window == nil)
    {
        self.view = nil;
        tabButtonsContainerView = nil;
        contentContainerView = nil;
        indicatorImageView = nil;
    }
}

- (void)reloadTabButtons
{
    [self removeTabButtons];
    [self addTabButtons];
    
}

- (void)addTabButtons
{
    NSUInteger index = 0;
    for (UIViewController *viewController in self.viewControllers)
    {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.tag = TagOffset + index;
        button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
        button.titleLabel.shadowOffset = CGSizeMake(0.0f, 1.0f);
        
        UIOffset offset = viewController.tabBarItem.titlePositionAdjustment;
        button.titleEdgeInsets = UIEdgeInsetsMake(offset.vertical, offset.horizontal, 0.0f, 0.0f);
        button.imageEdgeInsets = viewController.tabBarItem.imageInsets;
        [button setTitle:viewController.tabBarItem.title forState:UIControlStateNormal];
        [button setImage:viewController.tabBarItem.image forState:UIControlStateNormal];
        
        [button addTarget:self action:@selector(tabButtonPressed:) forControlEvents:UIControlEventTouchDown];
        
        [self deselectTabButton:button];
        [tabButtonsContainerView addSubview:button];
        
        ++index;
    }
}

-(void)initViewControllers{
    [self setViewControllers:[self configureAndGetViewControllers]];

}

-(void)updateViewControllers{
    [self updateViewControllersAnimated:NO];
}

-(void)updateViewControllersAnimated:(BOOL)animated{
   // [self setViewControllers:_customViewControllers animated:animated];
    
}


-(NSArray*)configureAndGetViewControllers{
    
    NSString *startStoryboardName = @"StructureOverViewInline";
    if(IDIOM==IPAD){
        startStoryboardName = @"StructureOverViewInline-iPad";
    }
    
    _startViewController = [StructureOverViewController loadNowFromStoryboard:startStoryboardName];
    _startViewController.view.tag = 0;
    
    UIImage *tabButton;
    CGSize scaledSize = CGSizeMake(25, 25);
    
    UIImage *tabBarItemImage;
    NSString *tabBarItemTitle;
    
    
    //// BEGIN tab 0
    // if(SINGLEPLACEID==0){
    NSString *gestgidStartStoryboardName = @"gestgidStart";
    if(IDIOM==IPAD){
        gestgidStartStoryboardName = @"gestgidStart-iPad";
    }
    
    UIViewController *svc0 = [gestgidStartViewController loadNowFromStoryboard:gestgidStartStoryboardName]; // should be master view for structure element 1
    
    NavigationViewController *vc0 = [[NavigationViewController alloc] initWithRootViewController:svc0];
    
    // set tab bar item
    //vc0.tabBarItem = [[UITabBarItem alloc] initWithTabBarSystemItem:UITabBarSystemItemSearch tag:0]; // do this in the view controller itself?
    
    tabButton = [UIImage imageNamed:@"sb_home.png"];
    
    UIGraphicsBeginImageContextWithOptions(scaledSize, NO, 0.0);
    [tabButton drawInRect:CGRectMake(0, 0, scaledSize.width, scaledSize.height)];
    tabBarItemImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // get title
    tabBarItemTitle = NSLocalizedString(@"Home", @"");
    
    vc0.tabBarItem = [[UITabBarItem alloc] initWithTitle:tabBarItemTitle image:tabBarItemImage tag:0];
    
    if(vc0!=nil && ![globals sharedInstance].terminalMode)
        [_customViewControllers addObject:vc0];
    //  }
    //// END tab 0
    
    
    //// BEGIN Last tab
    if(_startViewController.data.count>numOfTabs){
        NavigationViewController *vc3 = [[NavigationViewController alloc] initWithRootViewController:_startViewController];
        
        _startViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon"]
                                                                                                 style:UIBarButtonItemStylePlain target:_startViewController.revealViewController
                                                                                                action:@selector(revealToggle:)];
        
        tabButton = [UIImage imageNamed:@"sb_home.png"];
        
        UIGraphicsBeginImageContextWithOptions(scaledSize, NO, 0.0);
        [tabButton drawInRect:CGRectMake(0, 0, scaledSize.width, scaledSize.height)];
        tabBarItemImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        // get title
        tabBarItemTitle = NSLocalizedString(@"Stay", @"");
        
        // set tab bar item
        vc3.tabBarItem = [[UITabBarItem alloc] initWithTitle:tabBarItemTitle image:tabBarItemImage tag:numOfTabs];
        
        vc3.navigationItem.titleView = [NavigationTitle createNavTitle:tabBarItemTitle SubTitle:[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedPlaceName"]];
        
        _startViewController.navigationItem.titleView = [NavigationTitle createNavTitle:tabBarItemTitle SubTitle:[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedPlaceName"]];
        
        [_customViewControllers addObject:vc3];
    }
    else if(_startViewController.data.count>numOfTabs-1){
        UIViewController *svc = [_startViewController getViewControllerForIndex:numOfTabs-1 delegate:self];
        
        if(svc==nil)svc = [UIViewController load:@"LoadingViewController" fromStoryboard:@"Loading"];
        
        NavigationViewController *vc = [[NavigationViewController alloc] initWithRootViewController:svc];
        
        svc.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon"]
                                                                                style:UIBarButtonItemStylePlain target:svc.revealViewController
                                                                               action:@selector(revealToggle:)];
        
        // get / modify image
        tabButton = [_startViewController loadImageForItem:_startViewController.data[numOfTabs-1]];
        
        UIGraphicsBeginImageContextWithOptions(scaledSize, NO, 0.0);
        [tabButton drawInRect:CGRectMake(0, 0, scaledSize.width, scaledSize.height)];
        tabBarItemImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        // get title
        tabBarItemTitle = _startViewController.data[numOfTabs-1][@"name"];
        
        // set tab bar item
        vc.tabBarItem = [[UITabBarItem alloc] initWithTitle:tabBarItemTitle image:tabBarItemImage tag:numOfTabs];
        
        vc.navigationItem.titleView = [NavigationTitle createNavTitle:tabBarItemTitle SubTitle:[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedPlaceName"]];
        
        svc.title = tabBarItemTitle;
        
        [_customViewControllers addObject:vc];
    }
    //// END last tab
    //// END custom tabs
    
    
    
    
    // return the view controllers
    return _customViewControllers;
}

- (void)removeTabButtons
{
    while ([tabButtonsContainerView.subviews count] > 0)
    {
        [[tabButtonsContainerView.subviews lastObject] removeFromSuperview];
    }
}

- (void)layoutTabButtons
{
    NSUInteger index = 0;
    NSUInteger count = [self.viewControllers count];
    
    CGRect rect = CGRectMake(0.0f, 0.0f, floorf(self.view.bounds.size.width / count), self.tabBarHeight);
    
    indicatorImageView.hidden = YES;
    
    NSArray *buttons = [tabButtonsContainerView subviews];
    for (UIButton *button in buttons)
    {
        if (index == count - 1)
            rect.size.width = self.view.bounds.size.width - rect.origin.x;
        
        button.frame = rect;
        rect.origin.x += rect.size.width;
        
        if (index == self.selectedIndex)
            [self centerIndicatorOnButton:button];
        
        ++index;
    }
}

- (void)centerIndicatorOnButton:(UIButton *)button
{
    CGRect rect = indicatorImageView.frame;
    rect.origin.x = button.center.x - floorf(indicatorImageView.frame.size.width/2.0f);
    rect.origin.y = self.tabBarHeight - indicatorImageView.frame.size.height;
    indicatorImageView.frame = rect;
    indicatorImageView.hidden = NO;
}

- (void)setSelectedViewController:(UIViewController *)newSelectedViewController
{
    [self setSelectedViewController:newSelectedViewController animated:NO];
}

- (void)setSelectedViewController:(UIViewController *)newSelectedViewController animated:(BOOL)animated
{
    NSUInteger index = [self.viewControllers indexOfObject:newSelectedViewController];
    if (index != NSNotFound)
        [self setSelectedIndex:index animated:animated];
}

- (void)tabButtonPressed:(UIButton *)sender
{
    [self setSelectedIndex:sender.tag - TagOffset animated:YES];
}

#pragma mark - Change these methods to customize the look of the buttons

- (void)selectTabButton:(UIButton *)button
{
    [button setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    
    UIImage *image = [[UIImage imageNamed:@"MHTabBarActiveTab"] stretchableImageWithLeftCapWidth:0 topCapHeight:0];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    [button setBackgroundImage:image forState:UIControlStateHighlighted];
    
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setTitleShadowColor:[UIColor colorWithWhite:0.0f alpha:0.5f] forState:UIControlStateNormal];
}

- (void)deselectTabButton:(UIButton *)button
{
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    UIImage *image = [[UIImage imageNamed:@"MHTabBarInactiveTab"] stretchableImageWithLeftCapWidth:1 topCapHeight:0];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    [button setBackgroundImage:image forState:UIControlStateHighlighted];
    
    [button setTitleColor:[UIColor colorWithRed:175/255.0f green:85/255.0f blue:58/255.0f alpha:1.0f] forState:UIControlStateNormal];
    [button setTitleShadowColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

- (CGFloat)tabBarHeight
{
    return 44.0f;
}


-(void)iFeedbackModelDidLoadDataWithViewController:(UIViewController *)viewController forIndex:(NSUInteger)index{
    
    UIImage *tabButton;
    CGSize scaledSize = CGSizeMake(25, 25);
    
    UIImage *tabBarItemImage;
    NSString *tabBarItemTitle;
    
    int add = (SINGLEPLACEID==0)?1:0;
    
    UIViewController *svc = viewController;
    
    NavigationViewController *vc = [[NavigationViewController alloc] initWithRootViewController:svc];
    
    svc.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon"] style:UIBarButtonItemStylePlain target:svc.revealViewController action:@selector(revealToggle:)];
    
    
    // get / modify image
    tabButton = [_startViewController loadImageForItem:_startViewController.data[index]];
    
    UIGraphicsBeginImageContextWithOptions(scaledSize, NO, 0.0);
    [tabButton drawInRect:CGRectMake(0, 0, scaledSize.width, scaledSize.height)];
    tabBarItemImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // get title
    tabBarItemTitle = _startViewController.data[index][@"name"];
    
    // set tab bar item
    vc.tabBarItem = [[UITabBarItem alloc] initWithTitle:tabBarItemTitle image:tabBarItemImage tag:index+add];
    
    vc.title = tabBarItemTitle;
    
    
    vc.navigationItem.titleView = [NavigationTitle createNavTitle:tabBarItemTitle SubTitle:[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedPlaceName"]];
    
    //[_customViewControllers setObject:vc atIndexedSubscript:index+add];
    
    [self updateViewControllers];
}


@end
