//
//  WebViewController.h
//  pickalbatros
//
//  Created by User on 13.04.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>
#import "WebViewTouch.h"

@interface WebViewController : UIViewController <WKNavigationDelegate, WebViewTouchViewDelegate, UIWebViewDelegate>


//@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (strong, nonatomic) NSString *titleString;
@property (strong, nonatomic) NSURL *url;
@property (nonatomic) BOOL urlGiven, hideSharebutton;
@property (nonatomic) BOOL isLoading;
@property (nonatomic) BOOL hideSideBarButton;
@property (strong, nonatomic) NSString *currentURL;

@property (nonatomic) NSURLRequest *request;

@property (nonatomic) NSString *htmlString;
@property (nonatomic) BOOL useHTML;

-(void)hideModalWebView;

+(WebViewController*)getWebViewController;

@end

@interface WKWebView(SynchronousEvaluateJavaScript)

- (NSString *)stringByEvaluatingJavaScriptFromString:(NSString *)script;

@end
