//
//  globals.m
//  if_framework
//
//  Created by User on 12/4/15.
//  Copyright © 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "globals.h"

@interface globals ()

@property (nonatomic) localStorage *globalStorage;
@property (nonatomic) NSUserDefaults *standartdUserDefaults;

@end

@implementation globals


+(globals *)sharedInstance{
    //  Recommended way according to Apple
    static globals *sharedInstance = nil;
    static dispatch_once_t onceToken = 0;
    dispatch_once(&onceToken, ^
                  {
                      sharedInstance = [globals new];
                      sharedInstance.standartdUserDefaults = [NSUserDefaults standardUserDefaults];
                      sharedInstance.globalStorage = [localStorage storageWithFilename:@"globals"];
                      [sharedInstance loadGlobalVariables];
                  });
    
    return sharedInstance;
}


-(void)loadGlobalVariables{
    _pID = [[_globalStorage objectForKey:@"pID"] integerValue];
    _placeID = [[_globalStorage objectForKey:@"placeID"] integerValue];
    _campaignID = [[_globalStorage objectForKey:@"campaignID"] integerValue];
    _offerCampaignID = [[_globalStorage objectForKey:@"offerCampaignID"] integerValue];

    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    _terminalMode = [userDefaults boolForKey:@"terminalModeActive"];
    _userLoggedIn = [userDefaults boolForKey:@"userLoggedIn"];
    
    NSString *t = @"";
    if(IDIOM==IPAD)t=@"-t";
    _iFeedbackLink = [NSString stringWithFormat:@"%@/gestgid-%li%@",SYSTEM_DOMAIN,(long)_placeID,t];
    
    
    if(IFBCKPID!=0)
        _pID = IFBCKPID;
    
    if(SINGLEPLACEID!=0)
        _placeID = SINGLEPLACEID;
    
    if(FBCAMPAIGNID!=0)
        _campaignID = FBCAMPAIGNID;
    
    
    
    [_standartdUserDefaults setObject:[NSNumber numberWithInteger:_placeID] forKey:@"selectedPlaceID"];
    [_standartdUserDefaults synchronize];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUserDefaultValues) name:NSUserDefaultsDidChangeNotification object:nil];
    
    NSString *preferredLanguage = [NSLocale preferredLanguages][0];
    _currentLanguageKey = [preferredLanguage substringToIndex:2];
}

-(void)updateUserDefaultValues{
    
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    // check for updated terminal mode
    BOOL terminalMode = [userDefaults boolForKey:@"terminalModeActive"];
    if(_terminalMode!=terminalMode){
        // if terminal mode has changed update it and
        // post notification for updating view controllers
        _terminalMode=terminalMode;
        [[NSNotificationCenter defaultCenter] postNotificationName:TERMINAL_MODE_UPDATE object:nil];
    }
    
    _userLoggedIn = [userDefaults boolForKey:@"userLoggedIn"];
}


// custom getter to update link always (otherwise it might not contain changes to place id)
-(NSString*)getIFeedbackLink{
    
    NSString *t = @"";
    if(IDIOM==IPAD)t=@"-t";
    _iFeedbackLink = [NSString stringWithFormat:@"%@/gestgid-%li%@",SYSTEM_DOMAIN,(long)_placeID,t];
    return _iFeedbackLink;
}


// instance setters (setting value and storing in local .plist file)
-(void)setPID:(NSUInteger)pID{
    _pID = pID;
    [_globalStorage setObject:[NSNumber numberWithInteger:pID] forKey:@"pID"];
}

-(void)setPlaceID:(NSUInteger)placeID{
    _placeID = placeID;
    [_globalStorage setObject:[NSNumber numberWithInteger:placeID] forKey:@"placeID"];
}

-(void)setCampaignID:(NSUInteger)campaignID{
    _campaignID = campaignID;
    [_globalStorage setObject:[NSNumber numberWithInteger:campaignID] forKey:@"campaignID"];
}

-(void)setOfferCampaignID:(NSUInteger)offerCampaignID{
    _offerCampaignID = offerCampaignID;
    [_globalStorage setObject:[NSNumber numberWithInteger:offerCampaignID] forKey:@"offerCampaignID"];
}


// class methods to set IDs without having an instance (storing in local .plist file)
+(void)setPID:(NSUInteger)pID{
    [[localStorage storageWithFilename:@"globals"] setObject:[NSNumber numberWithInteger:pID] forKey:@"pID"];
}

+(void)setPlaceID:(NSUInteger)placeID{
    [[localStorage storageWithFilename:@"globals"] setObject:[NSNumber numberWithInteger:placeID] forKey:@"placeID"];
}

+(void)setCampaignID:(NSUInteger)campaignID{
    [[localStorage storageWithFilename:@"globals"] setObject:[NSNumber numberWithInteger:campaignID] forKey:@"campaignID"];
}

+(void)setOfferCampaignID:(NSUInteger)offerCampaignID{
    [[localStorage storageWithFilename:@"globals"] setObject:[NSNumber numberWithInteger:offerCampaignID] forKey:@"offerCampaignID"];
}

// for storing language while the app runs
-(void)setCurrentLanguageKey:(NSString*)languageKey{
    _currentLanguageKey = languageKey;
}

@end