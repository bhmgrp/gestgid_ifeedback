//
//  WebViewTouch.h
//  if_framework
//
//  Created by User on 6/3/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WebViewTouchViewDelegate <NSObject>

@optional -(void)webViewTouchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event;

@end

@interface WebViewTouch : UIWebView

@property (nonatomic) id<WebViewTouchViewDelegate> touchDelegate;

@end
