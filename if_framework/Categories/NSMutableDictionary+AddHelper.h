//
//  NSMutableDictionary+AddHelper.h
//  if_framework_terminal
//
//  Created by User on 4/13/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface  NSMutableDictionary (AddHelper)

-(void)addObjectIfNotNull:(id)object forKey:(NSString*)key;

-(void)addObjectFromUserDefaults:(NSString*)defaultsKey;

@end
