//
//  NSMutableDictionary+AddHelper.m
//  if_framework_terminal
//
//  Created by User on 4/13/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "NSMutableDictionary+AddHelper.h"

@implementation NSMutableDictionary (AddHelper)

-(void)addObjectIfNotNull:(id)object forKey:(NSString*)key{
    if(object!=nil){
        self[key] = object;
    }
}

-(void)addObjectFromUserDefaults:(NSString*)defaultsKey{
    [self addObjectIfNotNull:[[NSUserDefaults standardUserDefaults] objectForKey:defaultsKey] forKey:defaultsKey];
}

@end
