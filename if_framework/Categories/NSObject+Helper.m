//
//  NSObject+IsEmptyString.m
//  if_framework
//
//  Created by User on 3/14/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "NSObject+Helper.h"

@implementation NSObject (Helper)

-(BOOL)isEmptyObject{
    return self==nil || self == [NSNumber numberWithInt:0] || [self isEmptyString];
}

-(BOOL)isEmptyString{
    return self==nil || ([self isString] && ([(NSString*)self isEqualToString:@""] || [(NSString*)self isEqualToString:@"(null)"] || [(NSString*)self isEqualToString:@"0"]));
}

-(BOOL)isString{
    return [self isKindOfClass:[NSString class]];
}

@end
