//
//  NSDictionary+URLEncodedString.h
//  if_framework_terminal
//
//  Created by User on 4/13/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (URLEncodedString)

@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSString *urlEncodedString;

@end
