//
// Created by User on 6/3/16.
// Copyright (c) 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIViewController (MSLayoutSupport)

- (id<UILayoutSupport>)ms_navigationBarTopLayoutGuide;

- (id<UILayoutSupport>)ms_navigationBarBottomLayoutGuide;

@end