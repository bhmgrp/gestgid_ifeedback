//
//  startTopTabBar.m
//  if_framework
//
//  Created by User on 6/2/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "startTopTabBar.h"

@implementation startTopTabBar

+(instancetype)new{
    return [[self alloc] init];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        self.tintColor = [UIColor colorWithRGBHex:CUSTOMERTEXTCOLOR];
        self.itemPositioning = UITabBarItemPositioningFill;
        
        UITabBarItem  *barItem1 = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"All companies",@"") image:nil tag:0];
        
        UITabBarItem  *barItem2 = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"My company", @"") image:nil tag:1];
        
        
        [self setItems:@[barItem1,barItem2] animated:YES];
        
        [self setSelectedItem:self.items[0]];
        
        
        [[UITabBarItem appearanceWhenContainedIn:[self class], nil] setTitlePositionAdjustment:UIOffsetMake(0, -(49.0/2 - 16.0/2 - 2))];
        
        [[UITabBarItem appearanceWhenContainedIn:[self class], nil] setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue" size:16]} forState:UIControlStateNormal];
        
        [[UITabBarItem appearanceWhenContainedIn:[self class], nil] setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Bold" size:16]} forState:UIControlStateSelected];
        
        
        // separator
        UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2-0.25, 15, 0.5, 49-15-15)];
        
        separatorView.backgroundColor = [UIColor colorWithRGBHex:0x929292 withAlpha:1.0];
        
        [self addSubview:separatorView];
        
        
        // underline
        UIView *underlineView = [[UIView alloc] initWithFrame:CGRectMake(0, self.height-1, [UIScreen mainScreen].bounds.size.width, 1.0f)];
        
        underlineView.backgroundColor = [UIColor lightGrayColor];
        
        [self addSubview:underlineView];
        
        
        // highlight underline
        _highlightUnderlineView = [[UIView alloc] initWithFrame:CGRectMake(0, self.height-4, [UIScreen mainScreen].bounds.size.width/2-0.25, 4.0f)];
        
        _highlightUnderlineView.backgroundColor = [UIColor colorWithRGBHex:CUSTOMERTEXTCOLOR withAlpha:1.0];
        
        [self addSubview:_highlightUnderlineView];
    }
    
    return self;
}

-(instancetype)init{

    startTopTabBar *bar = [super init];
    
    bar.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 49);
    bar.tintColor = [UIColor colorWithRGBHex:CUSTOMERTEXTCOLOR];
    bar.itemPositioning = UITabBarItemPositioningFill;

    UITabBarItem  *barItem1 = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"All companies",@"") image:nil tag:0];
    
    UITabBarItem  *barItem2 = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"My company", @"") image:nil tag:1];
    
    
    [bar setItems:@[barItem1,barItem2] animated:YES];
    
    [bar setSelectedItem:bar.items[0]];
    
    
    [[UITabBarItem appearanceWhenContainedIn:[self class], nil] setTitlePositionAdjustment:UIOffsetMake(0, -(49.0/2 - 16.0/2 - 2))];
    
    [[UITabBarItem appearanceWhenContainedIn:[self class], nil] setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue" size:16]} forState:UIControlStateNormal];
    
    [[UITabBarItem appearanceWhenContainedIn:[self class], nil] setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Bold" size:16]} forState:UIControlStateSelected];
    
    
    // separator
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2-0.25, 15, 0.5, 49-15-15)];

    separatorView.backgroundColor = [UIColor colorWithRGBHex:0x929292 withAlpha:1.0];

    [bar addSubview:separatorView];


    // underline
    UIView *underlineView = [[UIView alloc] initWithFrame:CGRectMake(0, bar.height-1, [UIScreen mainScreen].bounds.size.width, 1.0f)];
    
    underlineView.backgroundColor = [UIColor lightGrayColor];
    
    [bar addSubview:underlineView];
    
    
    // highlight underline
    _highlightUnderlineView = [[UIView alloc] initWithFrame:CGRectMake(0, bar.height-4, [UIScreen mainScreen].bounds.size.width/2-0.25, 4.0f)];
    
    _highlightUnderlineView.backgroundColor = [UIColor colorWithRGBHex:CUSTOMERTEXTCOLOR withAlpha:1.0];
    
    [bar addSubview:_highlightUnderlineView];
    
    
    [bar layoutSubviews];
    [bar layoutIfNeeded];

    
    return bar;
}

+(UIView*)getTopBarView{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0,0, [UIScreen mainScreen].bounds.size.width,64)];

    startTopTabBar *bar = [[self alloc] init];

    bar.y = 20;

    [view addSubview:bar];

    return view;
}

@end
