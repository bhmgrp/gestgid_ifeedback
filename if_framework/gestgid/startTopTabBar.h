//
//  startTopTabBar.h
//  if_framework
//
//  Created by User on 6/2/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface startTopTabBar : UITabBar

@property (nonatomic) UIView *highlightUnderlineView;

+(instancetype)new;

-(instancetype)init;

+(UIView*)getTopBarView;

@end
