//
// Created by User on 6/7/16.
// Copyright (c) 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "DirectoryPlaceSelectViewController.h"
#import "gestgidTableViewCell.h"

#import "WebViewController.h"

@interface DirectoryPlaceSelectViewController ()

@property (nonatomic, strong) NSMutableData *downloadedData;

@property (nonatomic) NSMutableArray *searchResults;

@property (nonatomic) NSMutableArray *filteredResults;

@end

@implementation DirectoryPlaceSelectViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    
    self.title = @"Search";
    
    self.tableView.tableFooterView = [UIView new];
    
    [self setSidebarButton];
    [self loadData];
    [self loadSearchBar];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [_searchController setActive:NO];
    [_searchController.searchBar resignFirstResponder];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [_searchController.searchBar becomeFirstResponder];
    [self.tabBarController.tabBar setHidden:NO];
}

- (void)loadSearchBar{
    _searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    
    _searchController.searchResultsUpdater = self;
    _searchController.dimsBackgroundDuringPresentation = NO;

    _searchController.searchBar.delegate = self;

    _searchController.searchBar.searchBarStyle = UISearchBarStyleMinimal;

    _searchController.searchBar.keyboardAppearance = UIKeyboardAppearanceLight;
    _searchController.searchBar.showsCancelButton = YES;
    _searchController.searchBar.showsScopeBar = YES;
    _searchController.searchBar.placeholder = NSLocalizedString(@"Find company, city, country ...", @"");

    if (@available(iOS 11.0, *)) {
        self.navigationItem.searchController = _searchController;
        self.navigationItem.hidesSearchBarWhenScrolling = NO;
    } else {
        [self.navigationController setNavigationBarHidden:YES];
        self.view.backgroundColor = UIColor.whiteColor;
        _searchController.searchBar.backgroundColor = UIColor.whiteColor;
        _tableView.tableHeaderView = _searchController.searchBar;
    }
    
    _tableView.delegate = self;
    _tableView.dataSource = self;

    _searchController.definesPresentationContext = NO;
}

#pragma mark - table view delegate and datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _filteredResults.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    gestgidTableViewCell *c = [self.tableView dequeueReusableCellWithIdentifier:@"placeCell"];
    NSDictionary *placeDict = _filteredResults[indexPath.row];

    c.labelPlaceName.text = placeDict[@"name"];

    NSString *placeAddressString = @"";
    if(placeDict[@"city"] != nil &&  ![placeDict[@"city"] isEmptyString] && placeDict[@"cn_short_en"] != nil &&  ![placeDict[@"cn_short_en"] isEmptyString])
        placeAddressString = [NSString stringWithFormat:@"%@, %@",placeDict[@"city"],placeDict[@"cn_short_en"]];
    else if(placeDict[@"city"] != nil && ![placeDict[@"city"] isEmptyString])
        placeAddressString = placeDict[@"city"];
    else if(placeDict[@"cn_short_en"] != nil && ![placeDict[@"cn_short_en"] isEmptyString])
        placeAddressString = placeDict[@"cn_short_en"];

    c.labelPlaceAddress.text = placeAddressString;

    return c;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }

    // Prevent the cell from inheriting the Table View's margin settings
//    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
//        [cell setPreservesSuperviewLayoutMargins:NO];
//    }

    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

//    [self.tabBarController.tabBar setHidden:YES];
//    self.tabBarController.tabBar.y = [UIScreen mainScreen].bounds.size.height;

    NSCharacterSet *charactersToRemove = [[NSCharacterSet alphanumericCharacterSet] invertedSet];
    NSString *strippedReplacement = [[_filteredResults[indexPath.row][@"name"] componentsSeparatedByCharactersInSet:charactersToRemove] componentsJoinedByString:@"-"];

    NSError *error;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"(\\-)+" options:NSRegularExpressionCaseInsensitive error:&error];

    strippedReplacement = [regex stringByReplacingMatchesInString:strippedReplacement options:0 range:NSMakeRange(0, [strippedReplacement length]) withTemplate:@"-"];

    WebViewController *wvc = [WebViewController getWebViewController];
    wvc.url = [NSURL URLWithString:[NSString stringWithFormat:@"https://ifbck.com/directory/Bewertung-zu-%@",[self urlify:_filteredResults[indexPath.row][@"name"]]]];

    if([@0  isEqual: _filteredResults[indexPath.row][@"uid"]]){
        wvc.url = [NSURL URLWithString:@"https://ifbck.com/all"];
    }

    wvc.urlGiven = YES;
    wvc.titleString = _filteredResults[indexPath.row][@"name"];

    [self.navigationController pushViewController:wvc animated:YES];

    wvc.webView.y = 64;
    wvc.webView.height = [UIScreen mainScreen].bounds.size.height - 64;

    [_searchController setActive:NO];
}

- (NSString*)urlify:(NSString*)string{
    string = [[[[[[[string
                    stringByReplacingOccurrencesOfString:@"ß" withString:@"ss"]
                   stringByReplacingOccurrencesOfString:@"ä" withString:@"ae"]
                  stringByReplacingOccurrencesOfString:@"ö" withString:@"oe"]
                 stringByReplacingOccurrencesOfString:@"ü" withString:@"ue"]
                stringByReplacingOccurrencesOfString:@"Ä" withString:@"A"]
               stringByReplacingOccurrencesOfString:@"Ö" withString:@"O"]
              stringByReplacingOccurrencesOfString:@"Ü" withString:@"U"]
    ;
    
    string = [[string componentsSeparatedByCharactersInSet:[[NSCharacterSet alphanumericCharacterSet] invertedSet]] componentsJoinedByString:@"-"];
    string = [[NSRegularExpression regularExpressionWithPattern:@"(\\-)+" options:NSRegularExpressionCaseInsensitive error:nil] stringByReplacingMatchesInString:string options:0 range:NSMakeRange(0, [string length]) withTemplate:@"-"];
    
    return string;
}

#pragma mark - NSURLConnectionDataProtocol Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    // Initialize the data object
    _downloadedData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    // Append the newly downloaded data
    [_downloadedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{

    // Parse the JSON that came in
    NSError *error;
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:_downloadedData options:NSJSONReadingAllowFragments error:&error];

    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

    for(NSDictionary* dict in jsonArray){
        [_searchResults addObject:dict];
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{

    NSLog(@"Connection failed, Error: %@" ,error);
    [[[UIAlertView alloc] initWithTitle:nil message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
}

#pragma mark - loading places

- (void)loadData{
    _searchResults = [@[] mutableCopy];
    _filteredResults = [@[] mutableCopy];

    [self downloadItems];
}

- (void)downloadItems{
    // Download the json file

    // Create the request
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@get/directory",SECURED_API_URL]] cachePolicy:NSURLRequestReloadRevalidatingCacheData timeoutInterval:60.0];
    // Create the NSURLConnection
    [NSURLConnection connectionWithRequest:urlRequest delegate:self];
}


#pragma mark - UISearchResultsUpdating

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    // update the filtered array based on the search text
    NSString *searchText = searchController.searchBar.text;
    NSMutableArray *searchResults = [_searchResults mutableCopy];

    // strip out all the leading and trailing spaces
    NSString *strippedString = [searchText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

    // break up the search terms (separated by spaces)
    NSArray *searchItems = nil;
    if (strippedString.length > 0) {
        searchItems = [strippedString componentsSeparatedByString:@" "];
    }

    // build all the "AND" expressions for each value in the searchString
    //
    NSMutableArray *andMatchPredicates = [NSMutableArray array];

    for (NSString *searchString in searchItems) {
        // each searchString creates an OR predicate for: name, yearIntroduced, introPrice
        //
        // example if searchItems contains "iphone 599 2007":
        //      name CONTAINS[c] "iphone"
        //      name CONTAINS[c] "599", yearIntroduced ==[c] 599, introPrice ==[c] 599
        //      name CONTAINS[c] "2007", yearIntroduced ==[c] 2007, introPrice ==[c] 2007
        //
        NSMutableArray *searchItemsPredicate = [NSMutableArray array];

        // Below we use NSExpression represent expressions in our predicates.
        // NSPredicate is made up of smaller, atomic parts: two NSExpressions (a left-hand value and a right-hand value)

        // name field matching
        NSExpression *lhs = [NSExpression expressionForKeyPath:@"name"];
        NSExpression *rhs = [NSExpression expressionForConstantValue:searchString];
        NSPredicate *finalPredicate = [NSComparisonPredicate
                predicateWithLeftExpression:lhs
                            rightExpression:rhs
                                   modifier:NSDirectPredicateModifier
                                       type:NSContainsPredicateOperatorType
                                    options:NSCaseInsensitivePredicateOption];
        [searchItemsPredicate addObject:finalPredicate];

        // city matching
        lhs = [NSExpression expressionForKeyPath:@"city"];
        rhs = [NSExpression expressionForConstantValue:searchString];
        finalPredicate = [NSComparisonPredicate
                predicateWithLeftExpression:lhs
                            rightExpression:rhs
                                   modifier:NSDirectPredicateModifier
                                       type:NSContainsPredicateOperatorType
                                    options:NSCaseInsensitivePredicateOption];
        [searchItemsPredicate addObject:finalPredicate];

        // country matching
        lhs = [NSExpression expressionForKeyPath:@"cn_short_en"];
        rhs = [NSExpression expressionForConstantValue:searchString];
        finalPredicate = [NSComparisonPredicate
                predicateWithLeftExpression:lhs
                            rightExpression:rhs
                                   modifier:NSDirectPredicateModifier
                                       type:NSContainsPredicateOperatorType
                                    options:NSCaseInsensitivePredicateOption];
        [searchItemsPredicate addObject:finalPredicate];

        // country matching
        lhs = [NSExpression expressionForKeyPath:@"cn_short_local"];
        rhs = [NSExpression expressionForConstantValue:searchString];
        finalPredicate = [NSComparisonPredicate
                predicateWithLeftExpression:lhs
                            rightExpression:rhs
                                   modifier:NSDirectPredicateModifier
                                       type:NSContainsPredicateOperatorType
                                    options:NSCaseInsensitivePredicateOption];
        [searchItemsPredicate addObject:finalPredicate];


        // at this OR predicate to our master AND predicate
        NSCompoundPredicate *orMatchPredicates = [NSCompoundPredicate orPredicateWithSubpredicates:searchItemsPredicate];
        [andMatchPredicates addObject:orMatchPredicates];
    }

    // match up the fields of the Product object
    NSCompoundPredicate *finalCompoundPredicate =
            [NSCompoundPredicate andPredicateWithSubpredicates:andMatchPredicates];
    searchResults = [[searchResults filteredArrayUsingPredicate:finalCompoundPredicate] mutableCopy];

    _filteredResults = [searchResults mutableCopy];
    [_filteredResults addObject:@{@"uid":@(0),@"name":NSLocalizedString(@"Company not found?",@""),@"city":NSLocalizedString(@"Click here", @""),@"country":@""}];
    [self.tableView reloadData];
}

@end
