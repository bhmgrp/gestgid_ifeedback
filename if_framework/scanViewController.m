//
//  scanViewController.m
//  iFeedback
//
//  Created by Admin on 24.04.14.
//  Copyright (c) 2014 BHM Media Solutions. All rights reserved.
//

#import "scanViewController.h"
#import "WebViewController.h"
#import "SWRevealViewController.h"

@interface scanViewController ()
@property (nonatomic) BOOL isReading;


@property (nonatomic, strong) AVCaptureSession *captureSession;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *videoPreviewLayer;

-(void)stopReading;

@end

@implementation scanViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (!_isReading) {
        if ([self startReading]) {
        }
    }
}

-(void) viewDidAppear:(BOOL)animated {

            if ([self startReading]) {
                [_bbitemStart setTitle:@"Stop"];
                [_lblStatus setText:@"Scanning for QR Code..."];
            }

}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (BOOL)startReading {
    NSError *error;

    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
 
    
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
    
    if (!input) {
        NSLog(@"%@", [error localizedDescription]);
        return NO;
    }
    
    
    _captureSession = [[AVCaptureSession alloc] init];
    [_captureSession addInput:input];
    
    AVCaptureMetadataOutput *captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
    [_captureSession addOutput:captureMetadataOutput];
    
    dispatch_queue_t dispatchQueue;
    dispatchQueue = dispatch_queue_create("myQueue", NULL);
    [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatchQueue];
    [captureMetadataOutput setMetadataObjectTypes:[NSArray arrayWithObject:AVMetadataObjectTypeQRCode]];
    
    _videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_captureSession];
    [_videoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [_videoPreviewLayer setFrame:_viewPreview.layer.bounds];
    [_viewPreview.layer addSublayer:_videoPreviewLayer];
    
    [_captureSession startRunning];
    
    return YES;
}

-(void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection{
    if (metadataObjects != nil && [metadataObjects count] > 0) {
        AVMetadataMachineReadableCodeObject *metadataObj = [metadataObjects objectAtIndex:0];
        if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeQRCode]) {
            [_lblStatus performSelectorOnMainThread:@selector(setText:) withObject:[metadataObj stringValue] waitUntilDone:NO];
            
            self.scannedURL = [metadataObj stringValue];
            
            [_bbitemStart performSelectorOnMainThread:@selector(setTitle:) withObject:@"Start!" waitUntilDone:NO];
            _isReading = NO;
            
            [self performSelectorOnMainThread:@selector(stopReading) withObject:nil waitUntilDone:NO];
        }
        
    }
}

-(void)stopReading{
    [_captureSession stopRunning];
    _captureSession = nil;
    
    [_videoPreviewLayer removeFromSuperlayer];
    
    WebViewController *wvc = [WebViewController loadNowFromStoryboard:@"WebView"];
    wvc.url = [NSURL URLWithString:self.scannedURL];
    wvc.urlGiven = YES;
    
    wvc.hideSideBarButton = YES;
    wvc.titleString = @"iFeedback®";
    
    [self.navigationController pushViewController:wvc animated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //frames
    self.navigationItem.title = @"SCAN";
    
    UIBarButtonItem *stopReadingButton = [[UIBarButtonItem alloc]initWithTitle:@"Stop" style:UIBarButtonItemStylePlain target:self action:nil];
    [stopReadingButton setAction:@selector(stopReadingButton)];
    
    [self.revealMenuButton setTarget: self.revealViewController];
    [self.revealMenuButton setAction: @selector( revealToggle: )];
    
    
    _isReading = NO;
    
    _captureSession = nil;
    
    if (!_isReading) {
//        if ([self startReading]) {
//            [_bbitemStart setTitle:@"Stop"];
//            [_lblStatus setText:@"Scanning for QR Code..."];
//        }
    }
    else{
        [self stopReading];
    }
    
    _isReading = !_isReading;
}

- (IBAction)startStopReading:(id)sender {
    if (!_isReading) {
        if ([self startReading]) {
            [_bbitemStart setTitle:@"Stop"];
            [_lblStatus setText:@"Scanning for QR Code..."];
            _isReading = !_isReading;
        }
    }
    else{
        [self stopReading];
        [_bbitemStart setTitle:@"Start!"];
        _isReading = !_isReading;
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)stopReadingButton{
    [self stopReading];
}


@end
