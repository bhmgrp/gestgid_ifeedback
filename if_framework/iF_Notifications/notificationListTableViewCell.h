//
//  notificationListTableViewCell.h
//  Freese Gruppe
//
//  Created by User on 09.12.14.
//  Copyright (c) 2014 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface notificationListTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *cellTitle;
@property (strong, nonatomic) IBOutlet UITextView *cellDescription;

@end
