//
//  ViewController.h
//  localDBtest
//
//  Created by User on 05.12.14.
//  Copyright (c) 2014 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface notificationListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

- (IBAction)addRecord:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic) BOOL showSidebarButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;

@end

