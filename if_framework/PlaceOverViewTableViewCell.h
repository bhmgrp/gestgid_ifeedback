//
//  PlaceOverViewTableViewCell.h
//  pickalbatros
//
//  Created by User on 13.04.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlaceOverViewTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelPlaceName;
@property (weak, nonatomic) IBOutlet UILabel *labelPlaceAdress;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewPlaceNavigation;
@property (weak, nonatomic) IBOutlet UIView *viewImageAlphaOverlay;
@property (weak, nonatomic) IBOutlet UIView *titleBackgroundView;
@property (weak, nonatomic) IBOutlet UIView *shadowView;

@end
