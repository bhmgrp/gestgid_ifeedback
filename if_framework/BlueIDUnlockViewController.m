//
//  BlueIDUnlockViewController.m
//  if_framework
//
//  Created by User on 7/29/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "BlueIDUnlockViewController.h"

//#import <BlueIDSDKiOS/BlueIDSDKiOS.h>

@interface BlueIDUnlockViewController ()

@end

@implementation BlueIDUnlockViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.unlockButton addTarget:self
                          action:@selector(wasDragged:withEvent:)
                forControlEvents:UIControlEventTouchDragInside];
    [self.unlockButton addTarget:self
                          action:@selector(hasBeenDragged:withEvent:)
                forControlEvents:UIControlEventTouchUpInside|UIControlEventTouchUpOutside];
    
    [self slideToUnlockEffect];
}

- (void)slideToUnlockEffect{
    self.view.layer.backgroundColor = [[UIColor blackColor] CGColor];
    
    UIGraphicsBeginImageContext(self.slideToUnlockText.size);
    [[self.slideToUnlockText layer] renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *textImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    CGFloat textWidth = textImage.size.width;
    CGFloat textHeight = textImage.size.height;
    
    CALayer *textLayer = [CALayer layer];
    textLayer.contents = (id)[textImage CGImage];
    textLayer.frame = self.slideToUnlockText.frame;
    
    CALayer *maskLayer = [CALayer layer];
    
    // Mask image ends with 0.15 opacity on both sides. Set the background color of the layer
    // to the same value so the layer can extend the mask image.
    maskLayer.backgroundColor = [[UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.15f] CGColor];
    maskLayer.contents = (id)[[UIImage imageNamed:@"Mask.png"] CGImage];
    
    // Center the mask image on twice the width of the text layer, so it starts to the left
    // of the text layer and moves to its right when we translate it by width.
    maskLayer.contentsGravity = kCAGravityCenter;
    maskLayer.frame = CGRectMake(-textWidth-30, 0.0f, textWidth * 2, textHeight);
    
    // Animate the mask layer's horizontal position
    CABasicAnimation *maskAnim = [CABasicAnimation animationWithKeyPath:@"position.x"];
    maskAnim.byValue = [NSNumber numberWithFloat:textWidth+30];
    maskAnim.repeatCount = HUGE_VALF;
    maskAnim.duration = 2.0f;
    [maskLayer addAnimation:maskAnim forKey:@"slideAnim"];
    
    textLayer.mask = maskLayer;
    self.slideToUnlockText.alpha = 0.0;
    [self.unlockBackground.layer insertSublayer:textLayer below:self.unlockBackground.layer];
    
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.view.userInteractionEnabled = NO;
    self.unlockBackground.alpha = 0.5f;
    
//    [[BlueIDMobileDevice sharedInstance] synchronizeWithCompletionHandler:^(NSError *error)
//     {
//         NSArray *locks = [[BlueIDMobileDevice sharedInstance] getSecuredObjects];
//         NSLog(@"locks: %@", locks);
//         if(locks.count>0){
//             self.view.userInteractionEnabled = YES;
//             self.unlockBackground.alpha = 1.0f;
//         }
//     }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)wasDragged:(UIButton *)button withEvent:(UIEvent *)event {
    // get the touch
    UITouch *touch = [[event touchesForView:button] anyObject];
    
    // get delta
    CGPoint previousLocation = [touch previousLocationInView:button];
    CGPoint location = [touch locationInView:button];
    CGFloat delta_x = location.x - previousLocation.x;
    CGFloat delta_y = location.y - previousLocation.y;
    
    delta_y = 0;
    
    CGFloat newLoc_x = button.center.x + delta_x;
    
    
    //NSLog(@"%f",newLoc_x);
    
    if(newLoc_x < self.unlockButton.width/2+2){
        newLoc_x = self.unlockButton.width/2+2;
    } else if(newLoc_x > self.unlockBackground.width-self.unlockButton.width/2-2){
        newLoc_x = self.unlockBackground.width-self.unlockButton.width/2-2;
        //delta_y = location.y - previousLocation.y;
    }
    
    //self.backgroundView.hidden = NO;
    //self.backgroundView.alpha = (newLoc_x-100)/(260);
    
    //self.startButton.alpha = 1 - (newLoc_x-76)/(243-76);
    
    CGFloat newLoc_y = button.center.y + delta_y;
    
    // move button
    button.center = CGPointMake(newLoc_x,
                                newLoc_y);
    
    //float buttonMoved = newLoc_x - self.unlockButton.width/2+2;
    
    //self.slideToUnlockText.alpha = 1 - buttonMoved/(self.unlockBackground.width - self.unlockButton.width - 4);
    
}

- (void)hasBeenDragged:(UIButton *)button withEvent:(UIEvent *)event{
    CGFloat newLoc_x = button.center.x;
    
    if(newLoc_x >= self.unlockBackground.width-self.unlockButton.width/2-2){
        
        [self unlockLocks];
        
        UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityView.center = CGPointMake(self.unlockButton.width/2, self.unlockButton.height/2);
        [activityView startAnimating];
        [self.unlockButton addSubview:activityView];
        
        //self.unlockButton.transform = CGAffineTransformMakeRotation( ( 180 * M_PI ) / 360 );
        
        [UIView animateWithDuration:0.5 delay:3 options:0 animations:^{
            button.center = CGPointMake(self.unlockButton.width/2+2,button.center.y);
            //self.slideToUnlockText.alpha = 1.0f;
        } completion:^(BOOL finished) {
            if(finished)
                [activityView removeFromSuperview];
        }];
    }
    else {
        [UIView animateWithDuration:0.5 animations:^{
            button.center = CGPointMake(self.unlockButton.width/2+2,button.center.y);
            //self.slideToUnlockText.alpha = 1.0f;
        }];
    }
}

- (void) viewDidLayoutSubviews{
    self.unlockButton.layer.cornerRadius = self.unlockButton.height/2;
    
    self.unlockBackground.layer.cornerRadius = self.unlockBackground.height/2;
}

- (void)unlockLocks{
//    NSArray *securedObjects = [[BlueIDMobileDevice sharedInstance] getSecuredObjects];
//    
//    for(BlueIDSecuredObject *securedObject in securedObjects){
//        
//        NSLog(@"securedObject-ID: %@", securedObject);
//        
//        BlueIDCommand *command = [securedObject getCommands][0];
//        command = [securedObject getCommandById:@"tokn"];
//        BlueIDChannel *channel = [securedObject getChannelsForCommand:command][0];
//        
//        NSLog(@"command: %@", command);
//        NSLog(@"channel: %@", channel);
//        
//        [[BlueIDMobileDevice sharedInstance] executeCommand:command
//                                                  onChannel:channel
//                                            onSecuredObject:securedObject
//                                          completionHandler:^
//         (BlueIDCommandExecutionResponse *commandExecutionResponse, NSError *error)
//         {
//             if(error)
//                 NSLog(@"error: %@", error);
//             NSLog(@"commandResponse: %@", commandExecutionResponse);
//             NSLog(@"response code = %ld", (long)commandExecutionResponse.responseCode);
//         }];
//    }
}

@end
