//
//  WebViewTouch.m
//  if_framework
//
//  Created by User on 6/3/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "WebViewTouch.h"

@implementation WebViewTouch

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    NSLog(@"Touch web started");
    [super touchesBegan:touches withEvent:event];
    if([self.touchDelegate respondsToSelector:@selector(webViewTouchesBegan:withEvent:)]){
        [self.touchDelegate webViewTouchesBegan:touches withEvent:event];
    }
}

@end
