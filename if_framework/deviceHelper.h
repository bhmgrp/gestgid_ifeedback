//
//  DeviceHelper.h
//  if_framework_terminal
//
//  Created by User on 4/12/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface deviceHelper : NSObject

+(void)logDeviceInformation;

+(void)synchronizeDevice;

@end
