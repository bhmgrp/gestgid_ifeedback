//
//  BlueIDUnlockViewController.h
//  if_framework
//
//  Created by User on 7/29/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BlueIDUnlockViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *unlockButton;
@property (weak, nonatomic) IBOutlet UIView *unlockBackground;
@property (weak, nonatomic) IBOutlet UILabel *slideToUnlockText;

@end
