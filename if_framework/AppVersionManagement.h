//
//  AppVersionManagement.h
//  Pickalbatros
//
//  Created by User on 12.06.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppVersionManagement : NSObject <NSURLConnectionDelegate, UIAlertViewDelegate>
- (void)checkForUpdates;
-(void)updateAllData;
-(void)updateAvailable;
-(void)forceUpdateWithQuestion;
@end
