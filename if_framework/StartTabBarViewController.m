//
//  StartTabBarViewController.m
//  if_framework
//
//  Created by User on 3/4/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "StartTabBarViewController.h"

#import "gestgidStartViewController.h"

#import "NavigationViewController.h"
#import "StructureOverViewController.h"
#import "DirectoryPlaceSelectViewController.h"

#import "if_home.h"

#import "BlueIDUnlockViewController.h"

#import "WebViewController.h"

@interface StartTabBarViewController (){
    int numOfTabs;
}

@property (nonatomic) StructureOverViewController *startViewController;

@property (nonatomic) NSMutableArray *customViewControllers;

@property (nonatomic) BOOL hideFirstElement;

@end

@implementation StartTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [globals sharedInstance];
    
    numOfTabs = NUMBEROFTABSINTABVIEW;
    
    [self initStyle];

    self.delegate = self;
    
    _customViewControllers = [@[] mutableCopy];

    [self initViewControllers];
    
    // always select the last item (more page)
    if(self.selectedIndex!=0)
        [self setSelectedIndex:self.viewControllers.count-1];
    
}

-(void)initStyle{
    [self.tabBar setBarTintColor:[UIColor clearColor]];

    self.tabBar.backgroundColor = UIColor.blackColor;
    [self.tabBar setBarStyle:UIBarStyleDefault];
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIView *barBackground = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    [barBackground setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, self.tabBar.height)];
    
//    [self.tabBar addSubview:barBackground];

    UIView *bar = [[UIView alloc] init];

    bar.frame = CGRectMake(0,0, [UIScreen mainScreen].bounds.size.width,1);
    bar.backgroundColor = [UIColor lightGrayColor];

//    [self.tabBar addSubview:bar];

    //self.moreNavigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    //self.moreNavigationController.delegate = self;
    
    [[UIBarButtonItem appearance]setTintColor: [UIColor colorWithRGBHex:CUSTOMERTEXTCOLOR]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// method called, when we switch the tabs
-(void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    if(item.tag==0){
        //self.tabBar.hidden = 1;
    }
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
    if(tabBarController.selectedViewController!=viewController){
        [tabBarController.selectedViewController viewWillDisappear:YES];
    }

    return YES;
}

-(void)initViewControllers{
    [self setViewControllers:[self configureAndGetViewControllers] animated:YES];
    if(_customViewControllers.count==1){
        self.tabBar.hidden = YES;
    }
    else {
        self.tabBar.hidden = NO;
    }
}

-(void)updateViewControllers{
    [self updateViewControllersAnimated:NO];
}

-(void)updateViewControllersAnimated:(BOOL)animated{
    [self setViewControllers:_customViewControllers animated:animated];
    if(_customViewControllers.count==1){
        self.tabBar.hidden = YES;
    }
    else {
        self.tabBar.hidden = NO;
    }
}

-(NSArray*)configureAndGetViewControllers{
    
    NSString *startStoryboardName = @"StructureOverViewInline";
    if(IDIOM==IPAD){
        startStoryboardName = @"StructureOverViewInline-iPad";
    }
    
    _startViewController = [StructureOverViewController loadNowFromStoryboard:startStoryboardName];
    _startViewController.view.tag = 0;

    _customViewControllers = [[NSMutableArray alloc] init];

    UIImage *tabButton;
    CGSize scaledSize = CGSizeMake(25, 25);
    
    UIImage *tabBarItemImage;
    NSString *tabBarItemTitle;

    int index = 0;
    
    //// BEGIN tab 0
        NSString *gestgidStartStoryboardName = @"gestgidStart";
        if(IDIOM==IPAD){
            gestgidStartStoryboardName = @"gestgidStart-iPad";
        }
        
        UIViewController *svc0 = [gestgidStartViewController loadNowFromStoryboard:gestgidStartStoryboardName]; // should be master view for structure element 1
        
        NavigationViewController *vc0 = [[NavigationViewController alloc] initWithRootViewController:svc0];
    
        tabButton = [UIImage imageNamed:@"sb_home.png"];
    
        UIGraphicsBeginImageContextWithOptions(scaledSize, NO, 0.0);
        [tabButton drawInRect:CGRectMake(0, 0, scaledSize.width, scaledSize.height)];
        tabBarItemImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    
        // get title
        tabBarItemTitle = NSLocalizedString(@"Home", @"");
    
        vc0.tabBarItem = [[UITabBarItem alloc] initWithTitle:tabBarItemTitle image:tabBarItemImage tag:0];
        
        if(vc0!=nil && ![globals sharedInstance].terminalMode){
            [_customViewControllers addObject:vc0];
            index++;
        }
        else {
            _hideFirstElement = YES;
            numOfTabs++;
        }

    //// END tab 0

    //// BEGIN tab 1
    UIViewController *svc1 = [DirectoryPlaceSelectViewController loadNowFromStoryboard:@"DirectoryPlaceSelectViewController"];

    NavigationViewController *vc1 = [[NavigationViewController alloc] initWithRootViewController:svc1];
    tabButton = [UIImage imageNamed:@"ios_search.png"];

    UIGraphicsBeginImageContextWithOptions(scaledSize, NO, 0.0);
    [tabButton drawInRect:CGRectMake(0, 0, scaledSize.width, scaledSize.height)];
    tabBarItemImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    // get title
    tabBarItemTitle = NSLocalizedString(@"Search", @"");

    vc1.tabBarItem = [[UITabBarItem alloc] initWithTitle:tabBarItemTitle image:tabBarItemImage tag:0];

    if(vc1!=nil && ![globals sharedInstance].terminalMode)
        [_customViewControllers addObject:vc1];
    
    index++;
    //// END tab 1
    
   /* UIViewController *svc1 = [if_home loadNowFromStoryboard:@"if_home"]; // should be master view for structure element 1
    
    NavigationViewController *vc5 = [[NavigationViewController alloc] initWithRootViewController:svc1];
    
    // set tab bar item
    //vc0.tabBarItem = [[UITabBarItem alloc] initWithTabBarSystemItem:UITabBarSystemItemSearch tag:0]; // do this in the view controller itself?
    
    tabButton = [UIImage imageNamed:@"sb_home.png"];
    
    UIGraphicsBeginImageContextWithOptions(scaledSize, NO, 0.0);
    [tabButton drawInRect:CGRectMake(0, 0, scaledSize.width, scaledSize.height)];
    tabBarItemImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // get title
    tabBarItemTitle = NSLocalizedString(@"Home", @"");
    
    vc5.tabBarItem = [[UITabBarItem alloc] initWithTitle:tabBarItemTitle image:tabBarItemImage tag:0];
    
    if(vc5!=nil && ![globals sharedInstance].terminalMode)
        [_customViewControllers addObject:vc5]; */
    
    
    //// BEGIN custom tabs
    for(int i = 2;i<numOfTabs+2;i++){
        if(_startViewController.data.count>i-2){
            UIViewController *svc = [_startViewController getViewControllerForIndex:i-2 delegate:self];

            NavigationViewController *vc = [[NavigationViewController alloc] initWithRootViewController:svc];

            if([svc isKindOfClass:[WebViewController class]]){
                ((WebViewController*)svc).webView.scrollView.contentInset = UIEdgeInsetsMake(64, 0, 49, 0);
            }
            
            if(_customViewControllers.count <= index)
            _customViewControllers[index] = vc;

            svc.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon"]
                                                                                    style:UIBarButtonItemStylePlain target:svc.revealViewController
                                                                                   action:@selector(revealToggle:)];


            // get / modify image
            tabButton = [_startViewController loadImageForItem:_startViewController.data[i-2]];

            UIGraphicsBeginImageContextWithOptions(scaledSize, NO, 0.0);
            [tabButton drawInRect:CGRectMake(0, 0, scaledSize.width, scaledSize.height)];
            tabBarItemImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();

            // get title
            tabBarItemTitle = _startViewController.data[i-2][@"name"];

            // set tab bar item
            vc.tabBarItem = [[UITabBarItem alloc] initWithTitle:tabBarItemTitle image:tabBarItemImage tag:i+1];

            svc.title = tabBarItemTitle;
            vc.title = tabBarItemTitle;
            
            index++;
        }
    }
    
    //// BEGIN Last tab
        NavigationViewController *vc3 = [[NavigationViewController alloc] initWithRootViewController:_startViewController];
    
        _startViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon"]
                                                                                                 style:UIBarButtonItemStylePlain target:_startViewController.revealViewController
                                                                                                action:@selector(revealToggle:)];

        tabBarItemTitle = NSLocalizedString(@"More", @"");

        vc3.tabBarItem = [[UITabBarItem alloc] initWithTabBarSystemItem:UITabBarSystemItemMore tag:numOfTabs];
    
    vc3.title = @"MORE";

        _customViewControllers[index] = vc3;
    
    // return the view controllers
    return _customViewControllers;
}

- (void)setFrame:(CGRect)frame OfChildViewControllersForViewController:(UIViewController*) viewController{
    viewController.view.frame = frame;
    [viewController.view layoutIfNeeded];
    if(viewController.childViewControllers.count>0){
        for(UIViewController *vc in viewController.childViewControllers){
            [self setFrame:frame OfChildViewControllersForViewController:vc];
        }
    }
    if([viewController isKindOfClass:[UINavigationController class]]){
        for(UIViewController *vc in ((UINavigationController *)viewController).viewControllers){
            [self setFrame:frame OfChildViewControllersForViewController:vc];
        }
    }
    if([viewController isKindOfClass:[SWRevealViewController class]]){
        UIViewController *vc = ((SWRevealViewController *)viewController).frontViewController;
        [self setFrame:frame OfChildViewControllersForViewController:vc];
    }
}

- (void)ifbckDidLoadViewController:(UIViewController *)viewController atIndex:(NSInteger)index {
    [self setDefinesPresentationContext:YES];
    CGRect frame;

    frame = CGRectMake(0,0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - 49);
    UIViewController *vcWrapper = [UIViewController new];
    [vcWrapper.view setFrame: frame];
    UIView *containerView = [[UIView alloc] initWithFrame:frame];
    [vcWrapper.view addSubview:containerView];
    [vcWrapper.view setBackgroundColor:[UIColor whiteColor]];
    [vcWrapper addChildViewController:viewController];
    [vcWrapper setDefinesPresentationContext:YES];
    //[self setFrame:frame OfChildViewControllersForViewController:viewController];
    [containerView layoutSubviews];
    [containerView addSubview:viewController.view];
    [vcWrapper.view layoutSubviews];
    [viewController didMoveToParentViewController:vcWrapper];

    UIImage *tabButton;
    CGSize scaledSize = CGSizeMake(25, 25);

    UIImage *tabBarItemImage;
    NSString *tabBarItemTitle;
    tabButton = [_startViewController loadImageForItem:_startViewController.data[index]];

    UIGraphicsBeginImageContextWithOptions(scaledSize, NO, 0.0);
    [tabButton drawInRect:CGRectMake(0, 0, scaledSize.width, scaledSize.height)];
    tabBarItemImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    // get title
    tabBarItemTitle = _startViewController.data[index][@"name"];

    // set tab bar item
    vcWrapper.tabBarItem = [[UITabBarItem alloc] initWithTitle:tabBarItemTitle image:tabBarItemImage tag:index+1];
    
    if(_hideFirstElement)
        index = index-1;
    
    _customViewControllers[index+1] = vcWrapper;

    [self updateViewControllersAnimated:YES];
}

@end
