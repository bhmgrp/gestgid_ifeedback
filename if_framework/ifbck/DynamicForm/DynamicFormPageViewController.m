//
//  DynamicFormPageViewController.m
//  if_framework_terminal
//
//  Created by User on 7/15/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "DynamicFormPageViewController.h"
#import "DynamicFormViewController.h"

@interface DynamicFormPageViewController (){
    float startingViewHeight;
    float screenMultiplicator;
    float screenWidth;
    float screenHeight;
}

@property (nonatomic) NSString *languageKey;

@property (nonatomic) NSMutableArray *tableControllers;

@property (nonatomic) UIPageViewController *pageViewController;

@end

@implementation DynamicFormPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    screenHeight = [UIScreen mainScreen].bounds.size.height;
    screenWidth = [UIScreen mainScreen].bounds.size.width;
    screenMultiplicator =  screenWidth / 320.0f;
    
    
    _tableControllers = [@[] mutableCopy];

    _pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:@{}];

    _pageViewController.delegate = self;
    
    //_pageViewController.dataSource = self;
        
    [self.pageViewWrapper addSubview:_pageViewController.view];
    
    [self addChildViewController:_pageViewController];
    
    [_pageViewController didMoveToParentViewController:self];
    
    [self.pageViewWrapper layoutIfNeeded];

    [_pageViewController.view layoutIfNeeded];

    [self setDefinesPresentationContext:YES];
    
    [self initButtons];

    [self initTableData];
    
    [self registerForKeyboardNotifications];
    
    [self initNavigationBarButtons];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if(_languageKey!=nil){
        if(![_languageKey isEqualToString:[languages currentLanguageKey]]){
            _languageKey = [languages currentLanguageKey];
            
            [super.ifbckModel loadClient];
            
            [self initNavigationBarButtons];
        }
    }
    else {
        _languageKey = [languages currentLanguageKey];
    }

}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self updateButtonStyle];
}


- (void)initNavigationBarButtons{
    NSLog(@"init navigation bar buttons");
    // right bar button item
    NSArray *languagesArray = [(NSString*)super.ifbckModel.parameters[@"languages"] componentsSeparatedByString:@";"];
    if(languagesArray.count>1){
        NSLog(@"showing flag");
        //self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Language" style:UIBarButtonItemStylePlain target:self action:@selector(openLanguagesViewController)];
        NSString *flagKey = [languages currentLanguageKey];
        
        [_languageSelectorButton addTarget:self action:@selector(openLanguagesViewController) forControlEvents:UIControlEventTouchUpInside];
        
        _languageSelectorImageFlag.image = [UIImage imageNamed:[NSString stringWithFormat:@"flag_%@.gif",flagKey]];;
        
    }
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"   " style:UIBarButtonItemStylePlain target:self action:nil];
    
    //[self.navigationItem setHidesBackButton:YES animated:YES];
}

- (void)openLanguagesViewController{
    [self.navigationController pushViewController:[super.ifbckModel getLanguageSelectViewController] animated:YES];
}

- (void)viewDidLayoutSubviews {
    _pageViewController.view.frame = _pageViewWrapper.frame;
    
    [self.pageViewWrapper layoutIfNeeded];
    
    if(startingViewHeight == 0.0){
        startingViewHeight = self.view.height;
    }
    
}

- (void)initButtons{
    [_sendButton setTitle:[[super.ifbckModel getTranslationForKey:@"pi1_contactform_feedbacksubmit"] uppercaseString] forState:UIControlStateNormal];
    [_sendButton setTitleColor:[UIColor colorWithRGBHex:0x4682BE] forState:UIControlStateNormal];
    [_sendButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:22]];
    
    [_nextButton setTitle:[NSLocalizedStringFromTable(@"contact_next", @"ifbckLocalizable", @"") uppercaseString] forState:UIControlStateNormal];
    [_nextButton setTitleColor:[UIColor colorWithRGBHex:0x4682BE] forState:UIControlStateNormal];
    [_nextButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:22]];
    
    //[_previousButton setTitle:[NSLocalizedStringFromTable(@"contact_previous", @"ifbckLocalizable", @"") uppercaseString] forState:UIControlStateNormal];
    [_previousButton setTitleColor:[UIColor colorWithRGBHex:0x4682BE] forState:UIControlStateNormal];
    [_previousButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:22]];

    [self updateButtonStyle];
}

- (void)updateButtonStyle{
    
    // button background
    if(super.ifbckModel.parameters[@"app_send_button_background_color"]!= nil && ![super.ifbckModel.parameters[@"app_send_button_background_color"] isEmptyString]){
        // creating normal background
        _sendButton.backgroundColor = [UIColor colorFromHexString:super.ifbckModel.parameters[@"app_send_button_background_color"]];
        _nextButton.backgroundColor = [UIColor colorFromHexString:super.ifbckModel.parameters[@"app_send_button_background_color"]];
        _previousButton.backgroundColor = [UIColor colorFromHexString:super.ifbckModel.parameters[@"app_send_button_background_color"]];
    }
    else {
        _sendButton.backgroundColor = [UIColor colorWithRGBHex:0xffffff];
        _nextButton.backgroundColor = [UIColor colorWithRGBHex:0xffffff];
        _previousButton.backgroundColor = [UIColor colorWithRGBHex:0xffffff];
    }
    
    
    [_sendButton.layer setBorderColor:[UIColor colorWithRGBHex:0x4682BE].CGColor];
    [_sendButton.layer setBorderWidth:1.0f];
    [_sendButton.layer setCornerRadius:5.0f];
    
    [_nextButton.layer setBorderColor:[UIColor colorWithRGBHex:0x4682BE].CGColor];
    [_nextButton.layer setBorderWidth:1.0f];
    [_nextButton.layer setCornerRadius:5.0f];
    
    [_previousButton.layer setBorderColor:[UIColor colorWithRGBHex:0x4682BE].CGColor];
    [_previousButton.layer setBorderWidth:1.0f];
    [_previousButton.layer setCornerRadius:5.0f];
    
    
    // button text color
    if(super.ifbckModel.parameters[@"app_send_button_text_color"]!=nil && ![super.ifbckModel.parameters[@"app_send_button_text_color"] isEmptyString]){
        [_sendButton setTitleColor:[UIColor colorFromHexString:super.ifbckModel.parameters[@"app_send_button_text_color"]] forState:UIControlStateNormal];
        [_sendButton.layer setBorderColor:[UIColor colorFromHexString:super.ifbckModel.parameters[@"app_send_button_text_color"]].CGColor];
        
        [_nextButton setTitleColor:[UIColor colorFromHexString:super.ifbckModel.parameters[@"app_send_button_text_color"]] forState:UIControlStateNormal];
        [_nextButton.layer setBorderColor:[UIColor colorFromHexString:super.ifbckModel.parameters[@"app_send_button_text_color"]].CGColor];
        
        [_previousButton setTitleColor:[UIColor colorFromHexString:super.ifbckModel.parameters[@"app_send_button_text_color"]] forState:UIControlStateNormal];
        [_previousButton.layer setBorderColor:[UIColor colorFromHexString:super.ifbckModel.parameters[@"app_send_button_text_color"]].CGColor];
    }
    
    
    // button border color
    if(super.ifbckModel.parameters[@"app_send_button_border_color"]!=nil && ![super.ifbckModel.parameters[@"app_send_button_border_color"] isEmptyString]){
        [_sendButtonWrapperBackground setBackgroundColor:[UIColor colorFromHexString:super.ifbckModel.parameters[@"app_send_button_border_color"]]];
    }
    
    // button background
    //if(super.ifbckModel.parameters[@"app_send_button_background_color"]!= nil && ![super.ifbckModel.parameters[@"app_send_button_background_color"] isEmptyString]){
    //    _sendButtonBackground.backgroundColor = [UIColor colorFromHexString:super.ifbckModel.parameters[@"app_send_button_background_color"]];
    //    _sendButtonBackground.hidden  = NO;
    //    _sendButtonDefaultBackground.hidden = YES;
    //}
    
    
    // special iphone settings
    if(IDIOM!=IPAD){
        [_sendButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:18]];
        [_nextButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:18]];
        [_previousButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:18]];
        _sendButtonWrapperHeight.constant = 64;
    }
}

-(void)initTableData{

    NSMutableArray *tableData = [@[] mutableCopy];

    int i = 0;
    BOOL firstPage = YES;
    BOOL hasRequiredFields = NO;
    for(NSDictionary *dynamicField in self.ifbckModel.dynamicFields){

        if([dynamicField[@"required"] intValue]==1 || [dynamicField[@"required"] intValue]==2 || ![dynamicField[@"conditionalreqfields"] isEmptyObject]){
            hasRequiredFields = YES;
        }
        
        if([dynamicField[@"field_type"] intValue] == DYNAMICFIELD_PAGE && !firstPage){
            DynamicFormViewController *dfvc = [DynamicFormViewController loadNowFromStoryboard:@"DynamicFormViewController"];
            dfvc.pageViewController = self;
            dfvc.ifbckModel = self.ifbckModel;
            dfvc.tableData = tableData;
            dfvc.page = i;
            [dfvc loadTable];

            [_pageViewController addChildViewController:dfvc];

            [dfvc didMoveToParentViewController:_pageViewController];

            [_tableControllers addObject:dfvc];

            tableData = [@[] mutableCopy];
            i++;
        }
        else if([dynamicField[@"field_type"] intValue] != DYNAMICFIELD_PAGE){
            firstPage = NO;
            [tableData addObject:dynamicField];
        }
    }
    

    DynamicFormViewController *dfvc = [DynamicFormViewController loadNowFromStoryboard:@"DynamicFormViewController"];
    dfvc.pageViewController = self;
    dfvc.ifbckModel = self.ifbckModel;
    dfvc.tableData = tableData;
    dfvc.page = i;
    [dfvc loadTable];

    [_pageViewController addChildViewController:dfvc];

    [dfvc didMoveToParentViewController:_pageViewController];

    [_tableControllers addObject:dfvc];

    [_pageViewController setViewControllers:@[_tableControllers[0]] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:^(BOOL finished) {
        
    }];
    
    if(_tableControllers.count>1){
        _sendButton.hidden = YES;

        _nextButton.hidden = NO;
        [_nextButton addTarget:_tableControllers[0] action:@selector(nextButtonPressed) forControlEvents:UIControlEventTouchUpInside];

        _previousButton.hidden = NO;
        _previousButton.alpha = 0.5;
        _previousButton.userInteractionEnabled = NO;
        
//        if(!hasRequiredFields)
//            _pageViewController.dataSource = self;
    }
    else {
        _nextButton.hidden = YES;
        _previousButton.hidden = YES;
        ((DynamicFormViewController*)_tableControllers[0]).showNothingSelected = YES;
        [_sendButton addTarget:_tableControllers[0] action:@selector(sendFeedbackButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIViewController *)getViewControllerForPage:(int)page{
    if(_tableControllers.count > page && _tableControllers[page]!=nil)
        return _tableControllers[page];

    return nil;
}


-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController{
    return [self getViewControllerForPage:((DynamicFormViewController *)viewController).page+1];
}

-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController{
    return [self getViewControllerForPage:((DynamicFormViewController *)viewController).page-1];
}

-(void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray<UIViewController *> *)previousViewControllers transitionCompleted:(BOOL)completed{
    if(completed && finished){
        _currentPage = ((DynamicFormViewController*)pageViewController.viewControllers[0]).page;
        [self updateIndex];
    }
}


- (void)updateIndex {

    //next button
    [_nextButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
    if(_currentPage==_tableControllers.count-1){
        [_nextButton setTitle:[[super.ifbckModel getTranslationForKey:@"pi1_contactform_feedbacksubmit"] uppercaseString] forState:UIControlStateNormal];
        [_nextButton addTarget:_tableControllers[_currentPage] action:@selector(sendFeedbackButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    }
    else {
        [_nextButton setTitle:[NSLocalizedStringFromTable(@"contact_next", @"ifbckLocalizable", @"") uppercaseString] forState:UIControlStateNormal];
        [_nextButton addTarget:_tableControllers[_currentPage] action:@selector(nextButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    }
    
    // back button
    if(_currentPage>0){
        _previousButton.alpha = 0.7f;
        _previousButton.userInteractionEnabled = YES;
    }
    else {
        _previousButton.alpha = 0.5f;
        _previousButton.userInteractionEnabled = NO;
    }
    [_previousButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
    [_previousButton addTarget:_tableControllers[_currentPage] action:@selector(prevButtonPressed) forControlEvents:UIControlEventTouchUpInside];
}

- (void)showNextPage {
    if(_currentPage!=_tableControllers.count-1) {
        DynamicFormPageViewController *dfpvc = self;
        
        UIViewController *vc = [self getViewControllerForPage:_currentPage + 1];
        
        [vc.view endEditing:YES];
        [self.view endEditing:YES];
        
        if(vc!=nil)
            [_pageViewController setViewControllers:@[vc] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:^(BOOL finished){
                _currentPage = _currentPage + 1;
                [dfpvc updateIndex];
            }];
    }
}

- (void)showPrevPage {
    if(_currentPage!=0){
        DynamicFormPageViewController *dfpvc = self;
        
        UIViewController *vc = [self getViewControllerForPage:_currentPage - 1];
        
        [vc.view endEditing:YES];
        [self.view endEditing:YES];
        
        if(vc!=nil)
            [_pageViewController setViewControllers:@[vc] direction:UIPageViewControllerNavigationDirectionReverse animated:YES completion:^(BOOL finished){
                _currentPage = _currentPage - 1;
                [dfpvc updateIndex];
            }];
    }
}

- (void)sendFeedback {
    NSMutableArray *dynamicFields = [@[] mutableCopy];
    
    for(DynamicFormViewController *dfvc in _tableControllers)
        [dynamicFields addObjectsFromArray:dfvc.tableData];
    
    super.ifbckModel.dynamicFields = dynamicFields;
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        //send in background
        [super.ifbckModel sendFeedback];
        super.ifbckModel.languageSelected = NO;
    });
}



#pragma mark - Keyboard Notifications (for resizing view when keyboard appears)

- (void)registerForKeyboardNotifications{
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(keyboardWasShown:)
    //                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardDidChangeFrame:)
    //                                                 name:UIKeyboardWillChangeFrameNotification object:nil];
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:)
    //                                                 name:UIKeyboardWillShowNotification object:nil];
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:)
    //                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyBoardDidChangeFrame:(NSNotification*)aNotification{
    NSDictionary* info = aNotification.userInfo;
    
    CGSize kbSize = [info[UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    //self.view.height = [UIScreen mainScreen].bounds.size.height - (kbSize.height) + 60;
    
    self.navigationController.view.frame = CGRectMake(0, 0, screenWidth, startingViewHeight - (kbSize.height));
    NSLog(@"Keyboard Height: %f", kbSize.height);
    
    //self.headerView.height = 0;
    [self.view layoutIfNeeded];
}

- (void)keyboardWillBeShown:(NSNotification*)aNotification{
    NSLog(@"starting view height: %f", startingViewHeight);
    NSDictionary* info = aNotification.userInfo;
    
    CGSize kbSize = [info[UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    //self.view.height = [UIScreen mainScreen].bounds.size.height - (kbSize.height) + 60;
    
    self.navigationController.view.frame = CGRectMake(0, 0, screenWidth, startingViewHeight - (kbSize.height));
    
    
    //self.headerView.height = 0;
    [self.view layoutIfNeeded];
}

- (void)keyboardWasShown:(NSNotification*)aNotification{
    //    NSDictionary* info = [aNotification userInfo];
    //
    //    CGSize kbSize = [info[UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    //
    //    //self.view.height = [UIScreen mainScreen].bounds.size.height - (kbSize.height) + 60;
    //
    //    [UIView animateWithDuration:0.5 animations:^(void){
    //        self.navigationController.view.frame = CGRectMake(0, -(64+_headerView.height), [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - (kbSize.height) +_headerView.height+64);
    //    }];
    //
    //
    //    //self.headerView.height = 0;
    //    [self.view layoutIfNeeded];
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification{
    self.navigationController.view.frame = CGRectMake(0, 0, screenWidth, startingViewHeight);
    //self.view.height = [UIScreen mainScreen].bounds.size.height;
    [self.view layoutIfNeeded];
}

- (void)keyboardWillShow:(NSNotification *)notification{
    NSDictionary *userInfo = [notification userInfo];
    CGSize kbSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.30
                          delay:0.0
                        options:(7 << 16) // This curve ignores durations
                     animations:^{
                         self.navigationController.view.frame = CGRectMake(0, 0, screenWidth, startingViewHeight - (kbSize.height));
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
}

- (void)keyboardWillHide:(NSNotification *)notification{
    [UIView animateWithDuration:0.30
                          delay:0.0
                        options:(7 << 16) // This curve ignores durations
                     animations:^{
                         self.navigationController.view.frame = CGRectMake(0, 0, screenWidth, startingViewHeight);
                         [self.view layoutIfNeeded];
                         
                     }
                     completion:nil];
}

@end
