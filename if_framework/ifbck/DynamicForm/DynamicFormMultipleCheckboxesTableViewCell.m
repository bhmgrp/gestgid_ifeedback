//
//  DynamicFormTableViewCell.m
//  if_framework
//
//  Created by User on 31.08.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "DynamicFormMultipleCheckboxesTableViewCell.h"

@interface DynamicFormMultipleCeckboxesTableViewCell()


@end

@implementation DynamicFormMultipleCeckboxesTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    
    if(IDIOM!=IPAD || IFBCKINTEGRATED || TRUE){
        [self resizeFontFor:_label];
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)switchSwitched:(UISwitch*)sender {
    if([self.delegate respondsToSelector:@selector(switchSwitched:)]){
        [self.delegate switchSwitched:sender];
    }
}

@end
