//
//  DynamicFormPageViewController.h
//  if_framework_terminal
//
//  Created by User on 7/15/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iFeedbackViewController.h"

@interface DynamicFormPageViewController : iFeedbackViewController <UIPageViewControllerDelegate,UIPageViewControllerDataSource>

@property (weak, nonatomic) IBOutlet UIView *pageViewWrapper;

@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UIVisualEffectView *sendButtonDefaultBackground;
@property (weak, nonatomic) IBOutlet UIView *sendButtonBackground;
@property (weak, nonatomic) IBOutlet UIView *sendButtonWrapper;
@property (weak, nonatomic) IBOutlet UIView *sendButtonWrapperBackground;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sendButtonWrapperBottomSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sendButtonWrapperHeight;

@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIButton *previousButton;

@property (weak, nonatomic) IBOutlet UIImageView *languageSelectorImageFlag;
@property (weak, nonatomic) IBOutlet UIButton *languageSelectorButton;

@property (nonatomic) int currentPage;

- (void)showNextPage;
- (void)showPrevPage;
- (void)sendFeedback;

@end
