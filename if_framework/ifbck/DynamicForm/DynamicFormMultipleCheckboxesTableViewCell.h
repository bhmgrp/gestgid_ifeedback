//
//  DynamicFormTableViewCell.h
//  if_framework
//
//  Created by User on 31.08.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HCSStarRatingView.h"

#import "iFeedbackTableViewCell.h"

@protocol DynamicFormMultipleCheckboxesTableViewCellDelegate <NSObject>
-(void)switchSwitched:(UISwitch*)checkboxSwitch;
@end;

@interface DynamicFormMultipleCeckboxesTableViewCell : iFeedbackTableViewCell

@property (nonatomic) id<DynamicFormMultipleCheckboxesTableViewCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *label;

@property (weak, nonatomic) IBOutlet UISwitch *checkboxSwitch;


@end