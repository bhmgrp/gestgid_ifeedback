//
//  DynamicFormMultipleCheckboxesViewController.m
//  if_framework_terminal
//
//  Created by User on 6/29/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "DynamicFormMultipleCheckboxesViewController.h"

@interface DynamicFormMultipleCheckboxesViewController ()

@property (nonatomic) NSMutableArray *data;

@end

@implementation DynamicFormMultipleCheckboxesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    _data = [@[] mutableCopy];

    //NSLog(@" dfmc - multiple checkbox element: %@", _multipleCheckBoxElement);

    int i = 0;
    for(NSString *option in [_multipleCheckBoxElement[@"options"] componentsSeparatedByString:@";"]){
        if(i==0)
            _minValues = [option intValue];
        else if(i==1)
            _maxValues = [option intValue];
        else
            [_data addObject:option];

        i++;
    }

    _selectedValues = [@{} mutableCopy];
    if(_multipleCheckBoxElement[@"value"]!=nil && ![_multipleCheckBoxElement[@"value"] isEmptyString])
    for(NSString *selectedOptionString in [_multipleCheckBoxElement[@"value"] componentsSeparatedByString:@";"]){
        NSInteger selectedOption = [selectedOptionString integerValue];
        _selectedValues[[NSString stringWithFormat:@"%li",(long)(selectedOption)]] = @(selectedOption);
    }


    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.backgroundColor = [UIColor clearColor];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    DynamicFormMultipleCeckboxesTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];

    if([self saveValue:!cell.checkboxSwitch.on ForCheckbox:cell.checkboxSwitch]){
        [cell.checkboxSwitch setOn:!cell.checkboxSwitch.on animated:YES];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _data.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    //if(IDIOM!=IPAD)
        return 45.0f;

    //return 60.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DynamicFormMultipleCeckboxesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"checkbox_cell"];

    cell.label.text = _data[indexPath.row];

    cell.checkboxSwitch.tag = indexPath.row;

    [cell.checkboxSwitch setOn:_selectedValues[[NSString stringWithFormat:@"%li", (long) indexPath.row]] != nil animated:YES];

    cell.delegate = self;

    cell.backgroundColor = [UIColor clearColor];

    return cell;
}

- (CGSize)preferredContentSize {
    float height = [UIScreen mainScreen].bounds.size.height * 0.6f;
    float width = [UIScreen mainScreen].bounds.size.width * 1.0f;

    float tableViewContentHeight = [self tableView:self.tableView numberOfRowsInSection:0] * [self tableView:self.tableView heightForRowAtIndexPath:[NSIndexPath indexPathWithIndex:0]];

    if(tableViewContentHeight<height){
        height = tableViewContentHeight;
        _tableView.scrollEnabled = NO;
    }
    else {
        _tableView.scrollEnabled = YES;
    }

    return CGSizeMake(width, height);
}

- (void)switchSwitched:(UISwitch *)checkboxSwitch {
    if(![self saveValue:checkboxSwitch.on ForCheckbox:checkboxSwitch]){
        [checkboxSwitch setOn:!checkboxSwitch.on animated:YES];
    }
}

- (BOOL)saveValue:(BOOL)checkboxValue ForCheckbox:(UISwitch *)checkboxSwitch {
    if(checkboxValue) {
        if(_maxValues>_selectedValues.count) {
            _selectedValues[[NSString stringWithFormat:@"%li",(long)checkboxSwitch.tag]] = @(checkboxSwitch.tag);
            [self updateSelectedValueString];
            return YES;
        }
        else {
            [self showMaxValueReachedMessage];
            return NO;
        }
    }
    else {
        if(_minValues<_selectedValues.count) {
            [_selectedValues removeObjectForKey:[NSString stringWithFormat:@"%li",(long)checkboxSwitch.tag]];
            [self updateSelectedValueString];
            return YES;
        }
        else {
            [_selectedValues removeObjectForKey:[NSString stringWithFormat:@"%li",(long)checkboxSwitch.tag]];
            [self showMinValueNotReachedMessage];
            //return NO;
            [self updateSelectedValueString];
            return YES;
        }
    }
}

-(void)updateSelectedValueString{
    if(_selectedValues.count==0){
        _selectedValueString = nil;
        return;
    }
    
    if(_minValues>_selectedValues.count){
        [self.delegate disableOKButton];
    }
    else {
        [self.delegate enableOKButton];
    }
    
    NSMutableArray *selectedValueStrings = [@[] mutableCopy];
    for(NSNumber *selectedIndex in _selectedValues){
        [selectedValueStrings addObject:_data[[selectedIndex intValue]]];
    }

    _selectedValueString = [selectedValueStrings componentsJoinedByString:@", "];
}

-(void)showMinValueNotReachedMessage{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedStringFromTable(@"Error",@"ifbckLocalizable",@"Error") message:[NSString stringWithFormat:NSLocalizedStringFromTable(@"You should at least select %i item(s).",@"ifbckLocalizable",@""),_minValues] preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedStringFromTable(@"OK",@"ifbckLocalizable", @"") style:UIAlertActionStyleDefault handler:nil]];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)showMaxValueReachedMessage{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedStringFromTable(@"Error",@"ifbckLocalizable",@"Error") message:[NSString stringWithFormat:NSLocalizedStringFromTable(@"You should only select %i item(s).", @"ifbckLocalizable", @""), _maxValues] preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedStringFromTable(@"OK",@"ifbckLocalizable", @"") style:UIAlertActionStyleDefault handler:nil]];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
