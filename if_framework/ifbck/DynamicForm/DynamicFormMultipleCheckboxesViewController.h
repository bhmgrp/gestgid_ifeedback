//
//  DynamicFormMultipleCheckboxesViewController.h
//  if_framework_terminal
//
//  Created by User on 6/29/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DynamicFormMultipleCheckboxesTableViewCell.h"

@protocol DynamicFormCombinedCheckboxesDelegate <NSObject>

-(void)disableOKButton;
-(void)enableOKButton;

@end

@interface DynamicFormMultipleCheckboxesViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, DynamicFormMultipleCheckboxesTableViewCellDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic) NSMutableDictionary *multipleCheckBoxElement;

@property (nonatomic) NSMutableDictionary *selectedValues;

@property (nonatomic) int minValues, maxValues;

@property (nonatomic) NSString *selectedValueString;

@property (nonatomic) id<DynamicFormCombinedCheckboxesDelegate> delegate;


-(void)showMinValueNotReachedMessage;

-(void)showMaxValueReachedMessage;

@end
