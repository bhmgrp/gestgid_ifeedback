//
//  cronjob.h
//  if_framework_terminal
//
//  Created by User on 4/13/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface cronjob : NSObject

+(cronjob*)initCrojob;

@property (nonatomic) NSTimer *timer;

@property (nonatomic) iFeedbackModel *ifbckModel;

-(void)stopTimer;

-(void)startTimer;

-(void)restartTimer;

@end
