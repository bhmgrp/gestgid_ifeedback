//
//  ifbckNavigationViewController.h
//  pickalbatros
//
//  Created by User on 14.04.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iFeedbackViewController.h"

#import "TopicListViewController.h"

@interface ifbckNavigationViewController : UINavigationController <UITabBarDelegate>

- (instancetype)initWithRootViewController:(UIViewController *)rootViewController ifbckModel:(iFeedbackModel*)ifbckModel;

- (void)updateNavigationBarStyle;

- (void)hideNavigationBarBackground:(BOOL)hidden animated:(BOOL)animated;

- (void)fadeNavigationBarBackground:(BOOL)hidden animated:(BOOL)animated;

- (void)fadeNavigationBarBackground:(BOOL)hidden animated:(BOOL)animated finished:(void(^)(void))finished;

- (void)hideStatusBar:(BOOL)hide;

- (void)initHeaderView;
- (void)initFooterForTopics:(NSMutableArray*)topics;
- (void)showTopicFooter;

- (void)removeTopicFooter;

- (void)openDynamicForm;
- (void)openTopicRoot;

@property (nonatomic) TopicListViewController *rootTopicListViewController;

// properties
@property (nonatomic) UIView *footerView;
@property (nonatomic, strong) UITabBar *footerToolbar;

@property (nonatomic) UIView *topicOkButtonWrapper;
@property (nonatomic) UIView *topicOkButtonBackgroundView;
@property (nonatomic) UIView *topicOkButtonBackground;
@property (nonatomic) UIButton *topicOkButton;

@property (nonatomic) UIColor *textColor;

@property (nonatomic) float footerHeight;
@property (nonatomic) float NAVIGATION_BACKGROUND_HEIGHT;

@property (nonatomic) iFeedbackModel *ifbckModel;

-(void)updateBadge:(NSString*)badge ForIndex:(NSInteger)index;

@end
