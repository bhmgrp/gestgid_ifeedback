//
//  Feedback.m
//  if_framework_terminal
//
//  Created by User on 4/14/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "Feedback.h"

@implementation Feedback

- (void)log{
    NSLog(@" feedback - placeID: %i", self.placeID);
    NSLog(@" feedback - campaignID: %i", self.campaignID);
    NSLog(@" feedback - entryID: %i", self.entryID);
    NSLog(@" feedback - entryPointType: %i", self.entryPointType);
    NSLog(@" feedback - languageID: %i", self.languageID);
    NSLog(@" feedback - dynamicFieldsArray: %@", self.dynamicFieldsArray);
    NSLog(@" feedback - topicRatings: %@", self.topicRatings);
    NSLog(@" feedback - tstamp: %.0f", self.tstamp);
    NSLog(@" feedback - sent: %i", self.sent);
}

@end
