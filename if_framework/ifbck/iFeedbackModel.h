//
//  CampaignModel.h
//  if_framework
//
//  Created by User on 12/15/15.
//  Copyright © 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@protocol iFeedbackModelDelegate <NSObject>

@optional
-(void)iFeedbackModelDidLoadData;
@optional
-(void)iFeedbackModelDidLoadDataWithViewController:(UIViewController*)viewController forIndex:(NSUInteger)index;

@end

@interface iFeedbackModel : NSObject <
    NSURLConnectionDelegate
>

// properties
@property (nonatomic) id<iFeedbackModelDelegate> delegate;

@property (nonatomic) NSString *titleString;

@property (nonatomic) NSUInteger index; // for structure item

@property (nonatomic) float navigationBarHeight;

@property (nonatomic) BOOL languageSelected;

@property (nonatomic) NSString *entryPoint;

@property (nonatomic) NSString *entryURL;

// iFeedback data
@property (nonatomic) NSArray *surveyTopics;
@property (nonatomic) NSMutableDictionary *topicRatings;
@property (nonatomic) NSMutableArray *dynamicFields;

@property (nonatomic) NSDictionary *parameters;

@property (nonatomic) NSArray *places;
@property (nonatomic) NSArray *campaigns;

@property (nonatomic) NSDictionary *campaign;
@property (nonatomic) NSInteger campaignID;

@property (nonatomic) NSDictionary *place;
@property (nonatomic) NSInteger placeID;

@property (nonatomic) NSDictionary *entry;
@property (nonatomic) NSInteger entryID;

@property (nonatomic) NSInteger pID;

// properties to save view controllers
@property (nonatomic) UIViewController *startViewController;

@property (nonatomic) NSMutableDictionary *topicPages;

// loading functions
- (void)loadClient;

- (void)loadClientFromWeb;

- (void)loadClientUpdate;

- (void)checkForClientUpdate;

// functions to get controllers
- (UIViewController*)getDetailViewController;

- (UIViewController*)getPlaceSelectViewController;

- (UIViewController*)getCampaignSelectViewController;

- (UIViewController*)getTopicViewController;

- (UIViewController*)loadIFeedbackViewController;

- (UIViewController*)getLanguageSelectViewController;

// function to save feedbacks
- (NSDictionary*)sendFeedback;

// function that sends all feedbacks in the queue
- (BOOL)sendFeedbacksFromQueue;

// function returning a translation for a specific key
- (NSString*)getTranslationForKey:(NSString*)key;

// setter and getter
- (void)setEntryPoint:(NSString *)entryPoint;


@end
