//
//  iFeedbackViewController.h
//  if_framework_terminal
//
//  Created by User on 5/12/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface iFeedbackViewController : UIViewController

@property (nonatomic) UIColor *tableViewBackgroundColor, *tableViewCellBackgroundColor, *tableViewCellTintColor;

@property (nonatomic) BOOL hasNavigationBarbackground;

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttons;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *labels;
@property (strong, nonatomic) IBOutletCollection(UITextView) NSArray *textViews;

@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray *constraintsToShrinkBy3of4;
@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray *constraintsToShrinkBy1of2;
@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray *constraintsToShrinkBy1of4;

@property (nonatomic) iFeedbackModel *ifbckModel;

@end
