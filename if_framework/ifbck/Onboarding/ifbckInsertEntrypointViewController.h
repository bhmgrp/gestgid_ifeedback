//
//  InsertEntryPointViewController.h
//  if_framework_terminal
//
//  Created by User on 4/20/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "ifbckOnboardingViewController.h"

@interface ifbckInsertEntrypointViewController : ifbckOnboardingViewController <UITextFieldDelegate,UITextInputDelegate,UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *insertEntrypointLabel;

@property (weak, nonatomic) IBOutlet UILabel *hintButtonLabel;

@property (weak, nonatomic) IBOutlet UITextField *iFeedbackLinkTextfield;

- (IBAction)submitButton:(id)sender;

@end
