//
//  SelectConfigurationTypeViewController.m
//  if_framework_terminal
//
//  Created by User on 4/20/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "ifbckSelectConfigurationTypeViewController.h"

#import <AVFoundation/AVFoundation.h>
#import "ifbckInsertEntrypointViewController.h"
#import "ifbckScanEntrypointViewController.h"

@interface ifbckSelectConfigurationTypeViewController ()

@property (nonatomic) UIAlertView *restrictionsAlertView, *accessForCameraDeniedAlertView;

@end

@implementation ifbckSelectConfigurationTypeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)insertEntryPointButtonPressed:(id)sender {
    ifbckInsertEntrypointViewController *ievc = [ifbckInsertEntrypointViewController loadNowFromStoryboard:@"ifbckInsertEntrypointViewController"];
    
    //ievc.title = self.insertEntrypointButton.currentTitle;
    
    ievc.ifbckModel = super.ifbckModel;
    
    [self.navigationController pushViewController:ievc animated:YES];
}

- (IBAction)scanQRCodeButtonPressed:(id)sender {
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(authStatus == AVAuthorizationStatusAuthorized) {
        // do your logic
        [self openScanner];
    } else if(authStatus == AVAuthorizationStatusDenied){
        // denied
        [self askForCameraPermissions];
    } else if(authStatus == AVAuthorizationStatusRestricted){
        // restricted, normally won't happen
        
        _restrictionsAlertView = [[UIAlertView alloc] initWithTitle:NSLocalizedStringFromTable(@"Restricted",@"ifbckLocalizable", @"")
                                    message:NSLocalizedStringFromTable(@"The camera access was restricted for this device.\n\nPlease check the restrictions at: Settings > General > Restrictions > Camera and enable it.", @"ifbckLocalizable", @"")
                                   delegate:self
                          cancelButtonTitle:NSLocalizedStringFromTable(@"OK",@"ifbckLocalizable", @"")
                          otherButtonTitles:nil];
        _restrictionsAlertView.cancelButtonIndex = 1;
        [_restrictionsAlertView show];
        
    } else if(authStatus == AVAuthorizationStatusNotDetermined){
        // not determined?!
        [self askForCameraPermissions];
    } else {
        // impossible, unknown authorization status
        [self askForCameraPermissions];
    }
}

-(void)askForCameraPermissions{
    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
        if(granted){
            // access granted
            dispatch_async(dispatch_get_main_queue(), ^{
                [self openScanner];
            });
        } else {
            // access denied
            dispatch_async(dispatch_get_main_queue(), ^{
                _accessForCameraDeniedAlertView = [[UIAlertView alloc] initWithTitle:NSLocalizedStringFromTable(@"Camera not available",@"ifbckLocalizable", @"")
                                                                             message:NSLocalizedStringFromTable(@"You need to grant permissions for using the Camera in your settings.", @"ifbckLocalizable", @"")
                                                                            delegate:self
                                                                   cancelButtonTitle:NSLocalizedStringFromTable(@"Cancel",@"ifbckLocalizable", @"")
                                                                   otherButtonTitles:NSLocalizedStringFromTable(@"Settings",@"ifbckLocalizable", @""),nil];
                
                _accessForCameraDeniedAlertView.cancelButtonIndex = 1;
                [_accessForCameraDeniedAlertView show];
            });
        }
    }];
}

-(void)openScanner{
    ifbckScanEntrypointViewController *sevc = [ifbckScanEntrypointViewController loadNowFromStoryboard:@"ifbckScanEntrypointViewController"];
    
    sevc.title = [self.scanEntrypointButton.currentTitle uppercaseString];
    
    sevc.ifbckModel = super.ifbckModel;
    
    [self.navigationController pushViewController:sevc animated:YES];
}

-(void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex{
    NSLog(@" test %i, %i", (int)alertView.cancelButtonIndex, (int)buttonIndex);
    if(alertView == _accessForCameraDeniedAlertView){
        if(buttonIndex==1)
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@com.bhmms.ifeedback-terminal", UIApplicationOpenSettingsURLString]]];
    }
    if(alertView == _restrictionsAlertView){
        
    }
}

@end