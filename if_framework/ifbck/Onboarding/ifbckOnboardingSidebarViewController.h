//
//  OnboardingSidebarViewController.h
//  if_framework_terminal
//
//  Created by User on 4/25/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "ifbckOnboardingViewController.h"

@interface ifbckOnboardingSidebarViewController : ifbckOnboardingViewController <UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
