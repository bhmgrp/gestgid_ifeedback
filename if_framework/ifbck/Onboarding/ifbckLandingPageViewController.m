//
//  TerminalLandingPageViewController.m
//  if_framework_terminal
//
//  Created by User on 4/14/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import <StoreKit/StoreKit.h>


#import "ifbckLandingPageViewController.h"

#import "ifbckWebViewController.h"

#import "ifbckSelectConfigurationTypeViewController.h"

@interface ifbckLandingPageViewController () <SKProductsRequestDelegate, SKPaymentTransactionObserver>

@property (nonatomic) UIActivityIndicatorView *activityIndicatorView;

@end

@implementation ifbckLandingPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self translateOutlets];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)translateOutlets{
    //self.title = NSLocalizedStringFromTable(@"WELCOME", @"landing page - welcome message");
}

- (IBAction)openSettingsViewController:(id)sender {
    ifbckSelectConfigurationTypeViewController *sctvc = [ifbckSelectConfigurationTypeViewController loadNowFromStoryboard:@"ifbckSelectConfigurationTypeViewController"];
    
    //sctvc.title = self.alreadyCustomerButton.currentTitle;
    
    [self.navigationController pushViewController:sctvc animated:YES];
}

- (IBAction)openRegisterViewController:(id)sender {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedStringFromTable(@"Register now", @"ifbckLocalizable" ,@"") message:NSLocalizedStringFromTable(@"Um sich zu registrieren, benötigen Sie ein iFeedback® Lite Produkt-Abo.", @"ifbckLocalizable", @"") preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedStringFromTable(@"Abbonieren", @"ifbckLocalizable", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self wantsToRegister];
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedStringFromTable(@"Ich habe bereits ein Abo", @"ifbckLocalizable", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self restore];
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedStringFromTable(@"Cancel", @"ifbckLocalizable", @"") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }]];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)moreButtonPressed:(id)sender {
    ifbckWebViewController *wvc = [ifbckWebViewController getWebViewController];
    
    wvc.url = [NSURL URLWithString:@"http://ifeedback.de?headerless=1"];
    wvc.urlGiven = YES;
    wvc.titleString = [self.moreButton.currentTitle uppercaseString];
    
    [self.navigationController pushViewController:wvc animated:YES];
}

- (void)accountIsConfirmed{
    ifbckWebViewController *wvc = [ifbckWebViewController getWebViewController];
    
    wvc.url = [NSURL URLWithString:@"http://dev.ifbck.com/admin/user/registration-lite"];
    wvc.urlGiven = YES;
    wvc.titleString = [self.registerNewAccountButton.currentTitle uppercaseString];
    
    [self.navigationController pushViewController:wvc animated:YES];
}

#pragma mark - in-app-purchases

- (void)wantsToRegister{
    
    _activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [_activityIndicatorView setHidesWhenStopped:YES];
    [_activityIndicatorView startAnimating];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:_activityIndicatorView];
    
    NSLog(@"User requests to buy lite membership");
    
    if([SKPaymentQueue canMakePayments]){
        NSLog(@"User can make payments");
        
        //If you have more than one in-app purchase, and would like
        //to have the user purchase a different product, simply define
        //another function and replace kRemoveAdsProductIdentifier with
        //the identifier for the other product
        
        SKProductsRequest *productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithObject:@"com.bhmms.ifeedback_terminal.ifeedback_lite.1"]];
        productsRequest.delegate = self;
        [productsRequest start];
        
    }
    else{
        NSLog(@"User cannot make payments due to parental controls");
        //this is called the user cannot make payments, most likely due to parental controls
    }
}

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response{
    SKProduct *validProduct = nil;
    NSInteger count = [response.products count];
    if(count > 0){
        validProduct = [response.products objectAtIndex:0];
        NSLog(@"Products Available!");
        [self purchase:validProduct];
    }
    else if(!validProduct){
        NSLog(@"No products available");
        //this is called if your product id is not valid, this shouldn't be called unless that happens.
        [_activityIndicatorView stopAnimating];
    }
}

- (void)purchase:(SKProduct *)product{
    SKPayment *payment = [SKPayment paymentWithProduct:product];
    
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

- (IBAction) restore{
    //this is called when the user restores purchases, you should hook this up to a button
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

- (void) paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{
    BOOL restored = NO;
    NSLog(@"received restored transactions: %lu", (unsigned long)queue.transactions.count);
    for(SKPaymentTransaction *transaction in queue.transactions){
        if(transaction.transactionState == SKPaymentTransactionStateRestored){
            //called when the user successfully restores a purchase
            NSLog(@"Transaction state -> Restored");
            
            //if you have more than one in-app purchase product,
            //you restore the correct product for the identifier.
            //For example, you could use
            //if(productID == kRemoveAdsProductIdentifier)
            //to get the product identifier for the
            //restored purchases, you can use
            //
            //NSString *productID = transaction.payment.productIdentifier;
            restored = YES;
            [self accountIsConfirmed];
            [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
            break;
        }
    }
    [_activityIndicatorView stopAnimating];
    if(!restored){
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedStringFromTable(@"Du hast diesen Artikel derzeit nicht abboniert.DIDatePicker", @"ifbckLocalizable" ,@"") message:nil preferredStyle:UIAlertControllerStyleAlert];
        
        [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedStringFromTable(@"OK", @"ifbckLocalizable", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self wantsToRegister];
        }]];
        
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions{
    for(SKPaymentTransaction *transaction in transactions){
        switch(transaction.transactionState){
            case SKPaymentTransactionStatePurchasing: NSLog(@"Transaction state -> Purchasing");
                //called when the user is in the process of purchasing, do not add any of your own code here.
                break;
            case SKPaymentTransactionStatePurchased:
                //this is called when the user has successfully purchased the package (Cha-Ching!)
                [self accountIsConfirmed]; //you can add your code for what you want to happen when the user buys the purchase here, for this tutorial we use removing ads
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                NSLog(@"Transaction state -> Purchased");
                break;
            case SKPaymentTransactionStateRestored:
                NSLog(@"Transaction state -> Restored");
                //add the same code as you did from SKPaymentTransactionStatePurchased here
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                //called when the transaction does not finish
                if(transaction.error.code == SKErrorPaymentCancelled){
                    NSLog(@"Transaction state -> Cancelled");
                    //the user cancelled the payment ;(
                }
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            case SKPaymentTransactionStateDeferred:
                
                break;
        }
    }
    
    [_activityIndicatorView stopAnimating];
}
@end
