//
//  LanguageSelectViewController.h
//  if_framework_terminal
//
//  Created by User on 2/17/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "iFeedbackViewController.h"

@interface LanguageSelectViewController : iFeedbackViewController <UITableViewDataSource, UITableViewDelegate, iFeedbackModelDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic) NSArray *languages;


@property (strong, nonatomic) IBOutlet UIView *headerView;


@end
