//
//  ifbckGlobals.m
//  if_framework
//
//  Created by User on 12/4/15.
//  Copyright © 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "ifbckGlobals.h"

@interface ifbckGlobals ()

@property (nonatomic) localStorage *globalStorage;
@property (nonatomic) NSUserDefaults *standartdUserDefaults;

@end

@implementation ifbckGlobals


+(ifbckGlobals *)sharedInstance{
    //  Recommended way according to Apple
    static ifbckGlobals *sharedInstance = nil;
    static dispatch_once_t onceToken = 0;
    dispatch_once(&onceToken, ^
                  {
                      sharedInstance = [ifbckGlobals new];
                      sharedInstance.standartdUserDefaults = [NSUserDefaults standardUserDefaults];
                      sharedInstance.globalStorage = [localStorage storageWithFilename:@"ifbckGlobals"];
                      [sharedInstance loadGlobalVariables];
                  });
    
    return sharedInstance;
}


-(void)loadGlobalVariables{
    
    _sSYSTEM_DOMAIN = SYSTEM_DOMAIN;
    _sSECURED_API_URL = SECURED_API_URL;
    _sAPI_URL = API_URL;
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"appInTestingMode"]){
        _sSECURED_API_URL = @"http://dev.ifbck.com/scripts/api/";
        _sAPI_URL = @"http://dev.ifbck.com/scripts/api/";
        _sSYSTEM_DOMAIN = @"http://dev.ifbck.com";
    }
    
    NSString *preferredLanguage = [NSLocale preferredLanguages][0];
    _currentLanguageKey = [preferredLanguage substringToIndex:2];
    
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    _terminalMode = [userDefaults boolForKey:@"terminalModeActive"];
    _userLoggedIn = [userDefaults boolForKey:@"userLoggedIn"];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUserDefaultValues) name:NSUserDefaultsDidChangeNotification object:nil];
}

-(void)updateUserDefaultValues{
    
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    // check for updated terminal mode
    BOOL terminalMode = [userDefaults boolForKey:@"terminalModeActive"];
    if(_terminalMode!=terminalMode){
        // if terminal mode has changed update it and
        // post notification for updating view controllers
        _terminalMode=terminalMode;
        [[NSNotificationCenter defaultCenter] postNotificationName:TERMINAL_MODE_UPDATE object:nil];
    }
    
    
    _terminalMode = [userDefaults boolForKey:@"terminalModeActive"];
    _userLoggedIn = [userDefaults boolForKey:@"userLoggedIn"];
}


@end