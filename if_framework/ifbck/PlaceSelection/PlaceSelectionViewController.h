//
//  PlaceSelectionViewController.h
//  if_framework_terminal
//
//  Created by User on 5/23/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "iFeedbackViewController.h"
@interface PlaceSelectionViewController : iFeedbackViewController<UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) IBOutlet UIView *headerView;

@property (nonatomic) NSArray *places;

@property (nonatomic) NSDictionary *place;

@property (weak, nonatomic) IBOutlet UIButton *languageSelectorButton;

@property (weak, nonatomic) IBOutlet UIImageView *languageSelectorImageFlag;


@property (weak, nonatomic) IBOutlet UIView *footerBackgroundView;
@property (weak, nonatomic) IBOutlet UIImageView *footerImage;

@end
