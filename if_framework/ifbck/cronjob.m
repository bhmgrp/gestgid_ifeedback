//
//  cronjob.m
//  if_framework_terminal
//
//  Created by User on 4/13/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "cronjob.h"

@interface cronjob()

@property (nonatomic) BOOL workingOnQueue;

@end


@implementation cronjob

+(instancetype)initCrojob{
    cronjob *c = [self new];
    
    [c startTimer];
    
    [c performSelector:@selector(cronjobTasks) withObject:nil afterDelay:2.0];
    
    return c;
}

- (void)cronjobTasks{
    
    [self sendFeedbacksFromQueue];
    
    [self clientUpdate];
    
    [self synchronizeTerminal];
}

- (void)sendFeedbacksFromQueue{
    if(!_workingOnQueue){
        NSLog(@" cronjob - sending feedbacks from queue");
        _workingOnQueue = YES;
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            _workingOnQueue = ![self.ifbckModel sendFeedbacksFromQueue];
        });
    }
}

- (void)clientUpdate{
    NSLog(@" cronjob - check for updates");
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        [self.ifbckModel checkForClientUpdate];
    });
}

- (void)synchronizeTerminal{
    NSLog(@" cronjob - terminal synchronisation");
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        [deviceHelper synchronizeDevice];
    });
}



-(void)stopTimer{
    [_timer invalidate];
}

-(void)startTimer{
    [_timer invalidate];
    
    NSInteger interval = [[NSUserDefaults standardUserDefaults] integerForKey:@"cronjobTimer"];
    
    if(interval<1){
        interval = 1;
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"cronjobTimer"];
    }
    if(interval>24){
        interval = 24;
        [[NSUserDefaults standardUserDefaults] setInteger:24 forKey:@"cronjobTimer"];
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    interval = interval * 60 * 60;
    
    _timer = [NSTimer scheduledTimerWithTimeInterval:interval target:self selector:@selector(cronjobTasks) userInfo:nil repeats:YES];
}

-(void)restartTimer{
    [_timer invalidate];
    
    [self startTimer];
}
@end
