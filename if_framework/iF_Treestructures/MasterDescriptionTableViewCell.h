//
//  MasterDescriptionTableViewCell.h
//  KAMEHA
//
//  Created by User on 09.04.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MasterDescriptionTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextArea;

@end
