//
//  StructureTableViewCell.h
//  Database
//
//  Created by User on 17.10.14.
//  Copyright (c) 2014 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StructureTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@property (weak, nonatomic) IBOutlet UIImageView *cellImage;


@end
