//
//  MasterViewControllerNew.h
//  KAMEHA
//
//  Created by User on 08.04.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "homeModel.h"

@interface MasterViewControllerNew : UIViewController <UITableViewDataSource, UITableViewDelegate, homeModelProtocol, UISearchControllerDelegate,UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSURL *getJsonFiles;

@property (nonatomic) int elementID;
@property (nonatomic) int rootID;

@property (nonatomic) NSString *textAboveTheTable;
@property (nonatomic) NSString *cameFromMaster;
@property (nonatomic) NSString *elementName;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;

@property (weak, nonatomic) IBOutlet UIView *searchBarview;

@property (nonatomic) int viewType;

@property (nonatomic) BOOL isSubLevel;

// Search Bar
@property (weak, nonatomic) IBOutlet UIView *searchBarView;
@property (nonatomic) BOOL searchIsHidden;
- (void)hideSearchBar;


@property (weak, nonatomic) IBOutlet UIBarButtonItem *shareBarButtonItem;
@property (weak, nonatomic) IBOutlet UIImageView *shareButtonImage;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;
- (IBAction)shareButtonPressed:(id)sender;

@property (nonatomic) BOOL preloadImages;
@property (nonatomic) NSString *imageString;
@property (nonatomic) UIImage *cellImage;

@property (strong, nonatomic) IBOutlet UIView *webViewWrapper;
@property (weak, nonatomic) IBOutlet UIWebView *descriptionWebView;


@property (nonatomic) BOOL shouldReloadItems;
-(void)reloadItems;
@end
