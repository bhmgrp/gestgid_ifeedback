//
//  BookingViewTypeViewController.m
//  if_framework
//
//  Created by User on 3/8/16.
//  Copyright © 2016 BHM Media Solutions GmbH. All rights reserved.
//

#import "BookingViewTypeViewController.h"

#import "WebViewController.h"

#import "NavigationViewController.h"

@interface BookingViewTypeViewController ()

@end

@implementation BookingViewTypeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //
    _headerImageBorderView.layer.borderColor = [[UIColor whiteColor] CGColor];
    _headerImageBorderView.layer.borderWidth = 2.0f;
    _headerImageBorderView.layer.cornerRadius = 20.0f;
    
    //
    _headerImageIcon.image = [_headerImageIcon.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _headerImageIcon.tintColor = [UIColor whiteColor];
    
    if(_bookingDetail[@"text4"]!=nil && [_bookingDetail[@"text4"] isKindOfClass:[NSString class]] && ![[_bookingDetail[@"text4"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]){
        _text4Label.text = _bookingDetail[@"text4"];
    }
    else {
        _text4Label.text = @"10% RABATT IN DER HAUSBAR UND 20% IM SPA FÜR ALLE DIREKTBUCHER";
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)bookingButtonClicked:(id)sender {
    
    WebViewController *wvc = [WebViewController getWebViewController];
    
    wvc.url = [NSURL URLWithString:_bookingDetail[@"text5"]];
    wvc.urlGiven = YES;
    
    wvc.hideSideBarButton = YES;
    wvc.titleString = _bookingDetail[@"name"];
    
    NavigationViewController *nvc  = [[NavigationViewController alloc] initWithRootViewController:wvc];

    wvc.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:wvc action:@selector(hideModalWebView)];
    
    wvc.navigationItem.titleView = [NavigationTitle createNavTitle:wvc.titleString SubTitle:[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedPlaceName"]];
    
    
    [self presentViewController:nvc animated:YES completion:^(void){}];
}
@end
