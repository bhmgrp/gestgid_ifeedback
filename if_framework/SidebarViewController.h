//
//  SidebarViewController.h
//  pickalbatros
//
//  Created by User on 14.04.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"

#import "ifbck/iFeedbackModel.h"

@interface SidebarViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, NSURLConnectionDelegate, SWRevealViewControllerDelegate,iFeedbackModelDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSMutableData *downloadedData;

@property (nonatomic, strong) NSURL *jsonFileUrl;


@property (nonatomic) iFeedbackModel *iFeedbackModel;

@end
