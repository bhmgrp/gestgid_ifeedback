//
//  scanViewController.h
//  iFeedback
//
//  Created by Admin on 24.04.14.
//  Copyright (c) 2014 BHM Media Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>


@interface scanViewController : UIViewController <AVCaptureMetadataOutputObjectsDelegate>

@property (strong, nonatomic) IBOutlet UIBarButtonItem *revealMenuButton;

@property (weak, nonatomic) IBOutlet UIView *viewPreview;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *bbitemStart;

@property (strong, nonatomic) NSString* scannedURL;

- (IBAction)startStopReading:(id)sender;

@end
