//
//  WebViewController.m
//  pickalbatros
//
//  Created by User on 13.04.15.
//  Copyright (c) 2015 BHM Media Solutions GmbH. All rights reserved.
//

#import "WebViewController.h"
#import "SSKeychain.h"

@interface WebViewController ()

@property BOOL didLoad;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webViewBottomSpace;

@end

@implementation WebViewController

+(WebViewController*)getWebViewController{
    WebViewController *wvc = [WebViewController loadNowFromStoryboard:@"WebView"];
    
    return wvc;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    //[self loadCookies];
    
    self.webView.delegate = self;
    [self.webView setOpaque:YES];
    
    if(!self.urlGiven){
        self.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",SYSTEM_DOMAIN]]; //if it's not given use
    }
    
    //prepare request for the webView
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:self.url cachePolicy:NSURLRequestReloadRevalidatingCacheData timeoutInterval:30.0];

    if(self.request != nil){
        [self.webView loadRequest:request];
        self.isLoading = YES;
        return;
    }

    if(self.useHTML){
        // workaround but the only possible solution:
        // wrapping the content in a span, this sets a "default" font, if nothing is given in the html
        // we dont have access to modify the default font of the webkit
        _htmlString = [NSString stringWithFormat:@"<span style=\"font-family: %@;\">%@</span>",
         @"Helvetica",
         _htmlString];
        [self.webView loadHTMLString:self.htmlString baseURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",SYSTEM_DOMAIN]]];
    } else if(self.url!=nil){
        //load the request

        NSMutableString *cookieStringToSet = [[NSMutableString alloc] init];
        NSLog(@"self.url: %@", self.url);
        NSArray *cookiesToSet = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:self.url];
        for (NSHTTPCookie *cookie in cookiesToSet) {
            [cookieStringToSet appendFormat:@"%@=%@;", cookie.name, cookie.value];
        }

        if (cookieStringToSet.length) {
            [request setValue:cookieStringToSet forHTTPHeaderField:@"Cookie"];
        }

        [self.webView loadRequest:request];
        NSLog(@"%@",self.url);
        
        
        self.isLoading = YES;
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.title = self.titleString;
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {

    if([self.url.absoluteString isEqualToString:@"https://ifbck.com/admin/login"]){
        NSString *password = [self.webView stringByEvaluatingJavaScriptFromString:@"$('#loginform-password').val()"];
        NSString *username = [self.webView stringByEvaluatingJavaScriptFromString:@"$('#loginform-username').val()"];

        if(username != nil && ![username isEmptyString]){
            [[NSUserDefaults standardUserDefaults] setObject:username forKey:@"adminLoginUsername"];
            [[NSUserDefaults standardUserDefaults] synchronize];

            if(password != nil && ![password isEmptyString]){
                [SSKeychain setPassword:password forService:@"adminLogin" account:username];
            }
        }

    }

    return YES;
}

-(void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation{
    
    self.isLoading = YES;
    
    [miscFunctions showLoadingAnimationForView:self.view];
}

-(void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

-(void)webViewTouchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
}

-(void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation withError:(NSError *)error{
    
}

-(void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error{
    
    if(error.code==NSURLErrorCancelled){
        return;
    }
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    UIAlertView *webError = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Error",@"") message:[error localizedDescription] delegate:nil cancelButtonTitle:NSLocalizedString(@"Cancel",@"") otherButtonTitles: nil];
    
    [webError show];
    
    NSLog(@"%@", error);
}

- (void) webViewDidStartLoad:(UIWebView *)webView{
    
    self.isLoading = YES;

    [miscFunctions showLoadingAnimationForView:self.view];
}

- (void) webViewDidFinishLoad:(UIWebView *)webView{
    self.isLoading = NO;
    
    self.currentURL = self.webView.request.URL.absoluteString;
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

    if([self.url.absoluteString isEqualToString:@"https://ifbck.com/admin/login"]){
        NSString *username = [[NSUserDefaults standardUserDefaults] objectForKey:@"adminLoginUsername"];
        NSString *password = [SSKeychain passwordForService:@"adminLogin" account:username];

        NSString *jquery;

        NSLog(@"username: %@, password: %@", username, password);

        if(username!=nil && ![username isEmptyString]){
            jquery = [NSString stringWithFormat:@"$('#loginform-username').val('%@');",username];
            [self.webView stringByEvaluatingJavaScriptFromString:jquery];

            if(password != nil && ![password isEmptyString]){
                jquery = [NSString stringWithFormat:@"$('#loginform-password').val('%@');",password];
                [self.webView stringByEvaluatingJavaScriptFromString:jquery];
            }
        }


    }

    _didLoad = YES;
}

- (void) webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    if(error.code==NSURLErrorCancelled){
        return;
    }
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        UIAlertView *webError = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Error",@"") message:[error localizedDescription] delegate:nil cancelButtonTitle:NSLocalizedString(@"Cancel",@"") otherButtonTitles: nil];
        
        [webError show];
    
    NSLog(@"%@", error);
}

- (void)hideModalWebView{
    if(self.navigationController!=nil){
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
    else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)saveCookies {
    NSData *cookiesData = [NSKeyedArchiver archivedDataWithRootObject: [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:cookiesData forKey:[NSString stringWithFormat:@"cookiesForUrl%@",self.url.absoluteString]];
    [defaults synchronize];
}

- (void)loadCookies {
    NSArray *cookies = [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"cookiesForUrl%@",self.url.absoluteString]]];
    NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (NSHTTPCookie *cookie in cookies) {
        [cookieStorage setCookie: cookie];
    }
}

@end
@implementation NSURLRequest(DataController)
+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString *)host
{
    return NO;
}


@end

@implementation WKWebView(SynchronousEvaluateJavaScript)

- (NSString *)stringByEvaluatingJavaScriptFromString:(NSString *)script
{
    __block NSString *resultString = nil;
    __block BOOL finished = NO;
    
    [self evaluateJavaScript:script completionHandler:^(id result, NSError *error) {
        if (error == nil) {
            if (result != nil) {
                resultString = [NSString stringWithFormat:@"%@", result];
            }
        } else {
            NSLog(@"evaluateJavaScript error : %@", error.localizedDescription);
        }
        finished = YES;
    }];
    
    while (!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    
    return resultString;
}
@end
